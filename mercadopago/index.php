<?php
  require_once 'vendor/autoload.php';
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
  header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
  header("Allow: GET, POST, OPTIONS");

  if($_SERVER['REQUEST_METHOD'] == "GET")
    return print "403 - FORBIDDEN";
  
  function validate_form($form_data){
    $ret = true;
    try {
      $form_data = json_decode($form_data, true);
      if(is_array($form_data) && !empty($form_data)) {
        $payment_schema = ["name", "surname", "email", "address", "address_number", "neighborhood", "postal_code", "terms"];
        foreach($payment_schema as $key){
          if(!array_key_exists($key, $form_data)) {
            $ret =false ;
            break;
          } else if(empty($form_data[$key])) {
            $ret = false;
            break;
          }
        }
      } else {
        $ret = false;
      }
    } catch (Exception $e) {
      $ret = false;
    }
    return $ret;
  }

  if($_SERVER['REQUEST_METHOD'] == "POST"){

    $order_id = $_REQUEST["order_id"];
    $order_status = $_REQUEST["order_status"];
    return print $order_id ." - ". $order_status;

    if(!empty($_POST['total']))
      return print "400 - empty total amount";

    try {
      $products = json_decode($_POST['products'], true);
      if(!is_array($products) || empty($products)) {
        return print "400 - empty products";
      }
    } catch(Exception $e) {
      return print "error";
    }

    if(validate_form($_POST['formData'])){
      $token = $_REQUEST["token"];
      $payment_method_id = $_REQUEST["payment_method_id"];
      $installments = $_REQUEST["installments"];
      $issuer_id = $_REQUEST["issuer_id"];
      $payer_email = $_REQUEST["email"];
      
      MercadoPago\SDK::setAccessToken("TEST-2027691938455261-041423-6c0b4cd4e32d1892be85e9248b902d83-388173227");
      
      $payment = new MercadoPago\Payment();
      $payment->transaction_amount = 2000;
      $payment->token = $token;
      $payment->description = "Blue shirt";
      $payment->installments = $installments;
      $payment->payment_method_id = $payment_method_id;
      $payment->issuer_id = $issuer_id;
      $payment->payer = array(
        "email" => $payer_email
      );
      print_r($payment);
      $payment->save();
      return $payment->status;
    } else {
      return print "400 - BAD REQUEST";
    }
  }
?>