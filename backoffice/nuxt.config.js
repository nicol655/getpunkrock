export default {
  mode: 'spa',
  target: 'static',
  head: {
    title: "GetPunkRock - Backoffice",
    link: [
      {
        rel: "stylesheet",
        href: "//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons"
      },
      {
        rel: "icon",
        type: "image/ico",
        href: "/icon.ico"
      }
    ]
  },
  env: { ...process.env },
  auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      callback: false,
      home: '/'
    },
    strategies: {
      local: {
        scheme: 'refresh',
        refreshToken: {
          property: 'refresh_token'
        },
        endpoints: {
          login: {
            url: process.env.ENDPOINT_LOGIN_TOKEN_V1,
            method: 'post',
            propertyName: 'access'
          },
          user: {
            url: process.env.ENDPOINT_LOGIN_USER_V1,
            method: 'post',
            propertyName: 'user'
          },
          logout: { 
            url: process.env.ENDPOINT_LOGIN_LOGOUT,
            method: 'post' 
          },
          refresh: {
            url: process.env.ENDPOINT_REFRESH_TOKEN_V1,
            method: 'post'
          }
        }
      }
    }
  }
  ,buildModules: [
    '@nuxtjs/vuetify',
  ],
  modules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  css: ['./assets/scss/theme.scss'],
  styleResources: {
    scss: ['./assets/scss/*.scss']
  },
}