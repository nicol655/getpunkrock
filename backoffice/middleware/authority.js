export default ({ store, redirect }) => {
  if(store.$auth.$state.user.scope !== 'admin')
    return redirect('/error/403')
}