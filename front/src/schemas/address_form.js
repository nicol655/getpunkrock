const address_form = { 
  name: {
    name: "name",
    label: "Nombre",
    rules: [
      { required: true, message: "el nombre es requerido" },
      { pattern: /^[a-zA-ZñÑÀ-ú- ]+$/, message: "debe ingresar un nombre válido"}
    ],
    children: {
      placeholder: "Pablo"
    }
  },
  surname: {
    name: "surname",
    label: "Apellido",
    rules: [
      { required: true, message: "el apellido es requerido" },
      { pattern: /^[a-zA-ZñÑÀ-ú- ]+$/, message: "debe ingresar un apellido válido"}
    ],
    children: {
      placeholder: "Pérez"
    }
  },
  email: {
    name: "email",
    label: "Correo electrónico",
    rules: [
      { required: true, message: "el correo es requerido" },
      { type: "email", message: "debe ingresar un correo válido" }
    ],
    children: {
      placeholder: "micorreo@prueba.com"
    }
  },
  street: {
    name: "street",
    label: "Carrera y calle",
    rules: [
      { required: true, message: "la carrera y calle son requeridas"}
    ],
    children: {
      placeholder: "Carrera 8 N° 7-26"
    }
  },
  address_number: {
    name: "address_number",
    label: "Casa, piso, puerta",
    rules: [
      { required: true, message: "este campo es requerido"}
    ],
    children: {
      placeholder: "Casa 8"
    }
  },
  neighborhood: {
    name: "neighborhood",
    label: "Barrio",
    rules: [
      { required: true, message: "el barrio es requerido"}
    ],
    children: {
      placeholder: "Palermo"
    }
  },
  pcode: {
    name: "postal_code",
    label: "Código postal",
    rules: [
      { required: true, message: "el código postal es requerido" },
      { pattern: "^\\d{6}$", message: "debe ingresar un código válido" }
    ],
    children: {
      placeholder: "333410"
    }
  },
};

export default address_form;