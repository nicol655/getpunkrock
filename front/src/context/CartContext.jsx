import React, { useReducer, createContext } from "react";

const initialState = localStorage.getItem("gpr-cart")
  ? JSON.parse(localStorage.getItem("gpr-cart"))
  : {
      products: [],
      open: false,
      total_products: 0,
      total_amount: 0,
      product_list: [],
    };
function init(initialState) {
  return initialState;
}
function reducer(state, action) {
  switch (action.type) {
    case "open":
      return { ...state, open: state.products.length > 0 ? true: false };
    case "close":
      return { ...state, open: false };
    case "add":
      const add_qty = action.product.quantity;
      const index_occurrence = state.products.findIndex(
        (p) => p.id === action.product.id && p.selectedSubProduct === action.product.selectedSubProduct
      );
      if (index_occurrence >= 0) {
        state.products[index_occurrence].quantity += add_qty;
        const sp_ocurrence = state.product_list.findIndex(
          (sp) => sp.id === action.product.subProducts[action.product.selectedSubProduct].id
        );
        state.product_list[sp_ocurrence].quantity = state.products[index_occurrence].quantity;
      } else {
        state.products = [...state.products, action.product];
        state.product_list = [...state.product_list, {
          id: action.product.subProducts[action.product.selectedSubProduct].id,
          quantity: action.product.quantity
        }]
      }
      let total_add = state.total_products + add_qty;
      let total = state.total_amount + action.product.subProducts[action.product.selectedSubProduct].price * add_qty;
    
      state = { ...state, total_products: total_add, total_amount: total };
      break;
    case "remove":
      state = {
        ...state,
        total_products:
          state.total_products - state.products[action.index].quantity,
        total_amount:
          state.total_amount -
          state.products[action.index].quantity *
            state.products[action.index].subProducts[state.products[action.index].selectedSubProduct].price,
      };
      state.products.splice(action.index, 1);
      state.open = state.products.length > 0 ? true: false;
      break;
    default:
      break;
  }
  localStorage.setItem("gpr-cart", JSON.stringify({ ...state, open: false }));
  return { ...state };
}
export const CartContext = createContext(initialState);
export const CartProvider = (props) => {
  const [cart, setCart] = useReducer(reducer, initialState, init);
  return (
    <CartContext.Provider value={{ cart, setCart }}>
      {props.children}
    </CartContext.Provider>
  );
};
