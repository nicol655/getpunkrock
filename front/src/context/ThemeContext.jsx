const white = "#FAFAFA";
const black = "#212121";

const themeLight = {
  background: white,
  body: black,
};
const themeDark = {
  background: black,
  body: white,
};

const theme = (mode) => (mode === "dark" ? themeDark : themeLight);
export default theme;
