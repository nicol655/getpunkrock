import React, { useContext } from "react";
import { CartContext } from "../context/CartContext";

const useSetCart = ({ action, product }) => {
  const [cart, setCart] = useContext(CartContext);
  switch (action) {
    case "add":
      return setCart((curr) => [...curr, cart]);
    case "remove":
      return setCart((curr) => [...curr, cart]);
    default:
      throw new Error();
  }
};
export default useSetCart;
