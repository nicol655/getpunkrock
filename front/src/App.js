import React, { Fragment, lazy, Suspense } from "react";
import NavBar from "./components/NavBar/NavBar";
import Footer from "./components/Footer/Footer";
import Loader from "./components/Loader";
import { Route, Switch, useHistory } from "react-router-dom";
import HeaderFooterFixture from "./fixtures/HeaderFooter.json";
import NotMatchPage from "./containers/NoMatchPage";
import "react-image-gallery/styles/css/image-gallery.css";
import "./assets/css/base.css";

const Home = lazy(() => import("./containers/Home/Home"));
const Shop = lazy(() => import("./containers/Shop/Shop"));
const Contact = lazy(() => import("./containers/Contact"));
const Checkout = lazy(() => import("./containers/Checkout/Checkout"));
const Unsubscribe = lazy(() => import("./containers/Unsubscribe"));
const SingleProduct = lazy(() => import("./containers/SingleProduct/SingleProduct"));
const PaymentOk = lazy(()=> import("./containers/PaymentOk"));
const PaymentFailed = lazy(()=> import("./containers/PaymentFailed"));

function App() {
  const hide_for_future = false;
  let history = useHistory();
  const hide_cart = history.location.pathname.startsWith("/checkout") ? true : false;
  return (
    <Fragment>
      <Suspense fallback={<Loader />}>
        <div className="content--app">
          <NavBar fixture={HeaderFooterFixture.ES.header} hideCart={hide_cart}/>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path={"/(tienda|shop)/"}>
              <Shop />
            </Route>
            <Route path={"/cancelar-suscripcion/:hash"}>
              <Unsubscribe />
            </Route>
            {hide_for_future &&
              (<Route path={"/(contacto|contact)/"}>
                <Contact />
              </Route>)}
            <Route path={"/(producto|product)/:id/:name/:code"}>
              <SingleProduct />
            </Route>
            <Route path={"/(checkout|checkout)/"}>
              <Checkout />
            </Route>
            <Route exact path={"/payment/ok/:order"}>
              <PaymentOk />
            </Route>
            <Route path={"/payment/failed/"}>
              <PaymentFailed />
            </Route>
            <Route path="*">
              <NotMatchPage />
            </Route>
          </Switch>
        </div>
      </Suspense>
      <Footer />
    </Fragment>
  );
}

export default App;
