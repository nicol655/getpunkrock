import React, { useEffect, useState, useContext } from "react";
import { useParams } from "react-router-dom";
import { Result } from "antd";
import { CartContext } from "../context/CartContext";

import ProductCardSmall from "../components/ProductCard/ProductCardSmall";
import Button from "../components/Button/Button";

const PaymentOk = () => {
  const [orderInfo, setOrderInfo] = useState({});
  const { cart } = useContext(CartContext); //for testing purpose only
  const { order } = useParams();
  useEffect(()=>{
  },[])
  return(
    <> 
      <div className="app--container">
        <div>
          <Result
            status="success"
            title="Pago procesado exitosamente"
            subTitle={`Orden número: ${order}`}
            key="result payment ok"
            extra={[
              <Button  
              text="Imprimir"
              icon="mdi-printer"
              color="black"
              key="button print"
              onClick={()=>console.log("imprimir")}/>,
            ]}
          />
          <h3 className="order--title">Detalle de tu pedido:</h3>
          <div className="order--detail">
            {cart.products.map((p,i)=>{
              return(
                <ProductCardSmall
                  key={`product-order-info-${i}`}
                  cart_index={i}
                  product={{ ...p }}
                  quantity={p.quantity}
                  hideButton={true}
                />
              )
            })}
          </div>
        </div>
      </div>
      <style jsx>{`
        .order--detail {
          border: 1px solid #9e9e9e;
          padding: 15px;
        }
        .order--detail :global(.detail--price) {
          color: #212121;
        }
      `}</style>
    </>
  )
}
export default PaymentOk;