import React from "react";
import { Result, Button } from "antd";

const PaymentFailed = () => {
  return(
    <> 
      <div className="app--container">
        <div className="centered">
          <Result
              status="error"
              title="Pago rechazado"
              subTitle={`Lo sentimos, la orden no pudo ser procesada`}
              key="result payment failed"
              extra={[
                <Button key="failed volver">Volver</Button>,
              ]}
            />
        </div>
      </div>
      <style jsx>{`
        .centered {
          display: flex;
          flex-grow: 1;
          align-items: center;
        }
      `}</style>
    </>
  )
}
export default PaymentFailed;