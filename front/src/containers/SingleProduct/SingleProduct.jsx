import React, { useContext, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { CartContext } from "../../context/CartContext";
import { Button } from "antd";
import Slider from "../../components/Slider/Slider";
import SizeChart from "../../components/SizeChart";
import Accordion from "../../components/Accordion/Accordion";
import Media from "react-media";
const SingleProduct = () => {
  const { setCart } = useContext(CartContext);
  const [product, setProduct] = useState({});
  const [productQuantity, setProductQuantity] = useState(1);
  const [selectedSize, setSelectedSize] = useState(null);
  const [imageList, setImageList] = useState([{original:"", thumbnail:""}]);
  const [showModal, setShowModal] = useState({
    visible: false,
    title: "Producto agregado",
    content: "se ha agregado satisfactoriamente al carrito",
  });

  let { id } = useParams();
  const back_host = process.env.REACT_APP_BACKEND_ENDPOINT;
  let product_endpoint = `${process.env.REACT_APP_PRODUCTS_ENDPOINT}${id}/`;

  useEffect(() => {
    fetch(product_endpoint)
      .then((res) => res.json())
      .then((data) => {
        setProduct({ ...data });
        setProductImages(data.images);
      });
  }, [product_endpoint]);

  const setProductImages = (images) => {
    let images_list = [];
    images.map((img, i)=> {
      images_list.push({original : back_host + img.image_url , thumbnail: back_host + img.image_url});
    });
    setImageList(images_list);
  }

  const handleModal = () => {
    setShowModal((prevState) => {
      return { ...prevState, visible: !showModal.visible };
    });
  };

  const handleActiveOption = (e) => {
    setSelectedSize(e.target.name);
  };
  const handleChange = (e) => {
    setProductQuantity(parseInt(e.target.value));
  };
  
  return (
    <>
      <div className={`app--container product--info__container`}>
        <div className="info--content">
          <div className="image--container">
            {product.images?.length > 1 ? (
              <Media query={{ maxWidth: 768 }}>
                {(matches) =>
                  matches ? (
                    <Slider
                      additionalClass="product--gallery"
                      showPlayButton={false}
                      showThumbnails={false}
                      showBullets={true}
                      items={imageList}
                      showNav={false}
                    />
                  ) : (
                    <>
                      <Slider
                        additionalClass="product--gallery"
                        showPlayButton={false}
                        thumbnailPosition="left"
                        items={imageList}
                        showNav={false}
                      />
                    </>
                  )
                }
              </Media>
            ) : (
              <div>
                <img
                  alt="taller"
                  src={product?.images ? `${back_host}${product.images[0].image_url}` : ""}
                  title="producto principal"
                  loading="lazy"
                />
              </div>
            )}
          </div>

          <div className="product--info">
            <h1>{product.name}</h1>
            <h2>${product?.subProducts ? product?.subProducts[0]?.price : ""}</h2>
            <p className="description">
              {product.description}
            </p>
            <div className="product--options">
              <h5 className="product--subtitle">Tallas</h5>
              {product.size?.length && (
                <SizeChart
                  list={product.size}
                  onClick={handleActiveOption}
                  activeOption={selectedSize}
                />
              )}
              <button>
                <span className="mdi mdi-ruler"></span>Tabla de medidas
              </button>
            </div>
            <div className="add--product">
              {product?.subProducts && product?.subProducts[0].stock > 0 ? (
              <>
                <h5 className="product--subtitle">Agregar al carro</h5>
                <div className="product--input">
                    <input
                    min="1"
                    max={product?.subProducts[0].stock}
                    type="number"
                    defaultValue="1"
                    onKeyDown={(e) => e.preventDefault()}
                    onChange={handleChange}
                  />
                  <button
                  className="product--detail"
                  onClick={() => {
                    setCart({
                      type: "add",
                      product: { ...product, quantity: productQuantity },
                    });
                    handleModal();
                  }}
                  >
                  <span className="mdi mdi-cart-plus"></span>
                </button>
                
                {product?.subProducts && 
                product?.subProducts[0].stock < 20 && 
                product?.subProducts[0].stock > 20 ?  
                  <span className="warning--span">Quedan {product?.subProducts[0].stock} en stock!</span>
                : "" }
                
              </div>
              </>
              ) : (
                <div className="out--stock">
                  <span className="warning--span">Producto fuera de stock!</span>
                  <Button key="submit" className="button--confirm" type="primary" onClick={()=>{console.log("hacer pedido")}}>
                    Hacer pedido
                  </Button>
                </div>
              )}
            </div>
            <Accordion title="Detalle material">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Reprehenderit, velit voluptatem aliquam aperiam consequatur odio
              quisquam sint fugiat porro perspiciatis possimus sunt, impedit
              similique iste sit temporibus nostrum laudantium ex?
            </Accordion>
            <Accordion title="información de envio">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Reprehenderit, velit voluptatem aliquam aperiam consequatur odio
              quisquam sint fugiat porro perspiciatis possimus sunt, impedit
              similique iste sit temporibus nostrum laudantium ex?
            </Accordion>
          </div>
        </div>
      </div>
      <style jsx>{`
        .out--stock {
          display: flex;
          flex-direction: column;
          align-items: flex-start;
          margin-bottom: 5px;
        }
        .out--stock .warning--span {
          margin: 0 0 10px 0;
        }
        button,
        select {
          cursor: pointer;
        }
        .product--info__container {
          display: flex;
          flex-grow: 1;
          align-items: center;
          justify-content: center;
        }
        .info--content {
          display: flex;
          align-items: center;
          padding: 0 15px;
          justify-content: space-around;
        }
        .info--content :global(img) {
          flex-grow: 1;
          object-fit: contain;
          max-width: 600px;
        }
        .image--container,
        .product--info {
          display: flex;
          flex: 1 1 0px;
          justify-content: center;
        }
        .product--info {
          flex-grow: 1;
          max-width: 50vw;
          padding: 15px;
          display: flex;
          flex-direction: column;
          justify-content: center;
        }
        .product--info .description {
          margin: 10px 0;
        }
        .product--info input {
          width: 30px;
          margin-right: 10px;
        }
        h1 {
          margin: 0;
          font-family: Oswald-Medium;
        }
        h2 {
          margin: 0;
        }
        .add--product {
          display: block;
          align-items: center;
          margin-top: 10px;
        }
        .add--product input,
        select {
          width: 40px;
          padding: 0;
          background: #fafafa;
          height: 25px;
          margin-right: 5px;
          border: 1px solid #212121;
          text-align: center;
        }
        .add--product input:focus,
        select:focus {
          outline: none;
          border-color: #00fa00;
        }
        .product--input {
          display: flex;
        }
        .add--product button.product--detail {
          padding: 0;
          font-size: 18px;
          color: #00df00;
          font-weight: bold;
          align-self: flex-end;
        }
        .add--product button.product--detail .mdi {
          font-size: 21px;
        }
        .aditional--information h5 {
          text-transform: uppercase;
          font-size: 0.6em;
          position: relative;
          align-items: center;
          display: inline-flex;
          color: #3b913b;
        }
        .aditional--information h5:before,
        .aditional--information h5:after {
          content: "";
          display: block;
          position: absolute;
          width: 5px;
          height: 1px;
          background: #3b913b;
        }
        .aditional--information h5:before {
          left: -10px;
        }
        .aditional--information h5:after {
          right: -10px;
        }
        .aditional--information p {
          font-size: 12px;
          margin: 0;
        }
        .product--subtitle {
          margin: 5px 0;
        }
        .product--options button {
          padding: 0;
          color: #3b913b;
          font-size: 12px;
        }
        
        :global(.product--gallery
            .image-gallery-thumbnail.active, .product--gallery
            .image-gallery-thumbnail:hover) {
          border: 2px solid #00fa00;
          cursor: pointer;
        }
        :global(.product--gallery .image-gallery-icon:hover) {
          color: #00fa00;
        }
        @media screen and (max-width: 768px) {
          .product--info__container .info--content {
            padding: 0;
            flex-direction: column;
          }
          .product--info__container {
            padding-left: 0;
            padding-right: 0;
          }
          .product--info__container .info--content .product--info {
            max-width: 100%;
            flex: 1;
          }
          .image--container {
            flex: 1;
          }
        }
      `}</style>
    </>
  );
};
export default SingleProduct;
