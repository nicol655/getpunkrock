import React, { Fragment, useState, useEffect } from "react";
import { Tag, Form, Pagination } from 'antd';
import Media from "react-media";

import ProductCard from "../../components/ProductCard/ProductCard";
import Filter from "../../components/Filter/Filter";
import free_shipping from "../../assets/images/shipping_airplane.svg";
import safe_payment from "../../assets/images/safe_payment.svg";
import money_back from "../../assets/images/money_back.svg";
import chat_online from "../../assets/images/chat_online.svg";
import SearchBar from "../../components/SearchBar/SearchBar";
import BlankState from "../../components/BlankState";
import Toggle from "../../components/Toggle";
import ShopSkeleton from "../../components/ShopSkeleton";

const Shop = () => {
  const [products, setProducts] = useState([]);
  const [filtersMobilde, showFiltersMobile] = useState(false);
  const [form] = Form.useForm();
  const [filters, setFilters] = useState({});
  const [currentPage, setCurrentPage] = useState(1);
  const [totalProducts, setTotalProducts] = useState(10);
  const [loadingProducts, setLoadingProducts] = useState(true);
  const [matchQuery, setMatchQuery] = useState(false);
  const hide_for_future = false;
  const spanish_filters = {category: "Prenda", gender: "Género", brand: "Marca"};
  const products_per_page = matchQuery ? parseInt(process.env.REACT_APP_PRODUCTS_PER_PAGE_MOBILE) : parseInt(process.env.REACT_APP_PRODUCTS_PER_PAGE);
  const products_endpoint = process.env.REACT_APP_PRODUCTS_ENDPOINT;
  let current_page = localStorage.getItem("shop-page") ? parseInt(localStorage.getItem("shop-page")) : currentPage;
 
  useEffect(() => {
    fetch(`${products_endpoint}${current_page}/${products_per_page}${window.location.search}`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data.products);
        setTotalProducts(data.total);
        setLoadingProducts(false);
      });
  }, [form, current_page, products_endpoint, products_per_page]);

  useEffect(() => {
    let paramsString = window.location.search;
    let url = new URLSearchParams(paramsString);
    url.forEach((value, key)=>{
      form.setFieldsValue({[key]: value})
    });
    setFilters(form.getFieldsValue())
  },[form])

  useEffect(()=>{
    const media = window.matchMedia("(max-width: 768px)");
    const handleResize=()=> {
      if ( media.matches )
        setMatchQuery(true);
      else 
        setMatchQuery(false);
    }
    window.addEventListener("resize", handleResize)
    return () => window.removeEventListener("resize", handleResize);
  },[matchQuery])

  const handleFiltersMobile = () => {
    showFiltersMobile(!filtersMobilde);
  };

  const handleSearchQuery = (name, value, type="set") => {
    let paramsString = window.location.search;
    let url = new URLSearchParams(paramsString);
    if(type==="delete")
      url.delete(name);
    else
      url.set(name, value);
    window.location.search = url.toString();
  }

  const filterChange = (e) => {
    setFilters(form.getFieldsValue());
    getProducts(currentPage);
    handleSearchQuery(e.target.name, e.target.value);
  }

  const handleCloseChip = (name) => {
    handleSearchQuery(name, null, "delete");
  }

  const getProducts = (page) => {
    localStorage.setItem("shop-page", page);
    setCurrentPage(page);
    fetch(`${products_endpoint}${page}/${products_per_page}${window.location.search}`)
    .then((res) => res.json())
    .then((data) => {
      setProducts(data.products);
      setTotalProducts(data.total);
    })
  }
  return (
    <Fragment>
      <div className="app--container shop--container">
        <div className="shop--product__content">
          {filters !== undefined ? (
            <div className={`filter--list__active`}>
            {Object.keys(filters).map((f, i) => {
              return (
                <Tag key={`filter-chip-${i}`} closable 
                  visible={filters[f] !== undefined}
                  onClose={()=>handleCloseChip(f)}>
                    {`${spanish_filters[f]}: ${filters[f]}`}
                </Tag>
              );
            })}
          </div>
          ) : ""}
          
          <div
            className={`products--container ${
              filtersMobilde ? "show--filters__mobile" : "hide--filters__mobile"
            }`}
          >
            
            <Media
              query="(max-width: 768px)"
              render={() => (
                <div className="products--filter__actions">
                  <Toggle
                    active={filtersMobilde}
                    text="Filtros"
                    handleChange={handleFiltersMobile}
                  />
                  </div>
              )}
            />
            {hide_for_future && <SearchBar className="antd--searchbar" placeholder="Buscar producto"/>}

            <Form form={form} name="filter-form" >
              <h3 className="form--filter__title">Filtros</h3>
              <Filter
                filterChange={filterChange}
                handleFiltersVisibility={handleFiltersMobile}
              />
            </Form>
              {products.length > 0 && !loadingProducts ? (
                <div className="product--wrapper">
                  <div className="product--list">
                      {products.map((p, i) => {
                        return (
                          <ProductCard
                            key={`product_card_shop${i}`}
                            product={{ ...p }}
                          /> 
                        );
                      })}
                  </div>
                  <Pagination size="small" total={totalProducts} 
                    pageSize={products_per_page} 
                    current={current_page} onChange={getProducts}/>
                </div>
              ) : loadingProducts ? (
                <div className="product--list skeleton">
                  <ShopSkeleton size={products_per_page}/>
                </div>
              ) : ( 
                <div className="blankstate--container">
                  <BlankState title="Lo sentimos" description="No hemos encontrado resultados para la búsqueda" icon="mdi-emoticon-confused"/>
                </div>
              )}
          </div>
        </div>
        {hide_for_future && 
          <div className="shop--top__content">
            <div className="features__container">
              <div className="feature--content">
                <img src={free_shipping} alt="free shipping" />
                <div className="feature--content--desc">
                  <h4>Envío gratis</h4>
                  <p>En todos los pedidos</p>
                </div>
              </div>
              <div className="feature--content">
                <img src={chat_online} alt="online support" />
                <div className="feature--content--desc">
                  <h4>Soporte en línea</h4>
                  <p>En todos los pedidos</p>
                </div>
              </div>
              <div className="feature--content">
                <img src={money_back} alt="money back" />
                <div className="feature--content--desc">
                  <h4>Devolución de dinero</h4>
                  <p>En todos los pedidos</p>
                </div>
              </div>
              <div className="feature--content">
                <img src={safe_payment} alt="safe payment" />
                <div className="feature--content--desc">
                  <h4>Pago seguro</h4>
                  <p>En todos los pedidos</p>
                </div>
              </div>
            </div>
          </div>
        } 
      </div>
      <style jsx>{`
        .shop--product__content {
          display: flex;
          flex-direction: row;
          position: relative;
          flex-grow: 1;
          padding-top: 50px;
        }
        .shop--top__content {
          display: flex;
          flex-direction: column;
          justify-content: space-between;
        }
        .products--container {
          width: 100%;
          display: grid;
          grid-template-columns: 180px 1fr;
        }
        .filter--list__active {
          display: flex;
          align-items: center;
          flex-grow: 1;
          margin: 10px 0;
          align-items: center;
          position: absolute;
          top: 0;
          left: 0;
        }
        .product--wrapper {
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          margin-left: 50px;
        }
        .product--list {
          display: grid;
          grid-template-columns: repeat(5, 1fr);
          column-gap: 20px;
          row-gap: 20px;
          margin-bottom: 40px;
          margin-top: 10px;
          width: 100%;
          transition: width 0.5s linear;
        }
        .product--list.top {
          position: fixed;
          top: 15px;
        }
        .product--list.skeleton {
          height: 100%;
        }
        .blankstate--container {
          display: flex;
          flex-direction: column;
          justify-content: center;
        }
        .product--wrapper :global(.ant-pagination) {
          text-align: center;
          margin: 10px 0;
        }
        .products--container :global(.switch) {
          position: relative;
        }
        .filter--list__active .filter--active {
          padding: 2px 5px;
          align-items: center;
          display: flex;
          justify-content: center;
          border-radius: 23px;
          border: 2px solid #808080;
          color: #808080;
          margin: 0 2px;
        }
        .filter--list__active .filter--active span {
          font-weight: bold;
          font-size: 14px;
        }
        .filter--list__active .filter--active label {
          font-size: 13px;
        }
        .products--filter__actions {
          display: flex;
          grid-area: 1 / 1 / span 1 / span 2;
          justify-content: flex-end;
        }
        :global(.antd--searchbar) {
          margin: 20px 0;
          flex: 0 1 50%;
        }
        .filter--list__active :global(.ant-tag){
          margin-bottom: 7px;
        }
        .shop--container {
          width: 100%;
        }
        :global(.blank--state__container){
          align-self: center;
          width: 100%;
          max-width: 600px;
        }
        @media screen and (max-width: 1024px) {
          .shop--container .product--list {
            grid-template-columns: repeat(3, 1fr);
          }
        }
        @media screen and (max-width: 768px) {
          .blankstate--container {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
          }
          .products--container {
            grid-template-columns: 1fr;
          }
          .products--filter__actions {
            display: flex;
            justify-content: space-between;
          }
          :global(.antd--searchbar) {
            flex: 0 1 70%;
          }
          .shop--product__content {
            padding-top: 20px;
            flex-direction: column;
          }
          .filter--list__active {
            flex-wrap: wrap;
            position: relative;    
            margin: 0;
            top: 0;
          }
          .product--wrapper {
            margin-left: 0;
          }
          .form--filter__title {
            display: none;
          }
        }
        @media screen and (max-width: 600px) {
          .shop--product__content {
            padding-top: 0px;
          } 
          .shop--container .product--list {
            grid-template-columns: repeat(2, 1fr);
          }
        }
        @media screen and (max-width: 425px) {
          .shop--container .product--list {
            grid-template-columns: 1fr;
          }
          .features__container {
            grid-template-columns: 1fr;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default Shop;
