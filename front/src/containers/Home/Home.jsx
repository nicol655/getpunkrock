import React, { Fragment, useEffect, useRef, useState } from "react";
import first_section_img from "../../assets/images/banner.jpg";
import HiddenImage from "../../components/HiddenImage/HiddenImage";
import ProductCard from "../../components/ProductCard/ProductCard";
import Newsletter from "../../components/Newsletter/Newsletter";
import SocialGrid from "../../components/SocialGrid/SocialGrid";

const Home = () => {
  const [products, setProducts] = useState([]);
  const [animate, setAnimate] = useState("");
  const [hiddenImage, setHiddenImage] = useState(first_section_img);
  const grid = useRef(null); 
  const products_endpoint = process.env.REACT_APP_PRODUCTS_ENDPOINT;
  const hidden_image_endpoint = process.env.REACT_APP_HIDDEN_IMAGE_ENDPOINT;
  useEffect(() => {
    const screen = document.querySelector("body");
    if(screen) {
      screen.addEventListener("scroll", () => {
        if(grid.current){
          const animateGrid = grid.current.offsetTop - grid.current.offsetHeight;
          const duration = grid.current.offsetHeight;
          if (screen.scrollTop <= animateGrid) setAnimate("frame1");
          if (screen.scrollTop >= animateGrid + duration / 5) setAnimate("frame2");
          if (screen.scrollTop >= animateGrid + duration / 4) setAnimate("frame3");
          if (screen.scrollTop >= animateGrid + duration / 3) setAnimate("frame4");
        }
      });
      return () => screen.removeEventListener("scroll", () => {});
    }
  },[]);

  useEffect(() => {
    fetch(`${products_endpoint}latest_created/4/`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, [products_endpoint]);

  useEffect(() => {
    fetch(hidden_image_endpoint)
    .then((res) => res.json())
    .then((data) => {
      if(data.length > 0)
        setHiddenImage(data[0].image_url);
      
    });
  }, [hidden_image_endpoint]);
  return (
    <Fragment>
      <div className="app--container">
        <h1 className="section--title">Novedades</h1>
        <div className="section">
          <div className="home--top__products">
            {products.map((p, i) => {
                return (
                  <div className="single--products" key={`home_product_nov_${i}`}>
                    <ProductCard product={{ ...p }} />
                  </div>
                );
              })}
          </div>
        </div>

        <Newsletter />
        <h1 className="section--title">#PUNKROCK</h1>
        <div className={`home--social__container section`}>
          <div ref={grid}>
            <SocialGrid frame={animate}/>
          </div>
        </div>
        <div className="home--hidden__product section">
          <HiddenImage
            columns={6}
            rows={4}
            text="descubre"
            image={hiddenImage}
            parallax={true}
          />
        </div>
      </div>
      <style jsx>
        {`
          .home--top__products .single--products {
            display: flex;
          }
          .home--top__products {
            display: grid;
            justify-content: center;
            grid-template-columns: repeat(4, 1fr);
            grid-template-rows: repeat(1, 1fr);
            column-gap: 20px;
          }
          .home--top__products img {
            width: 100%;
            object-fit: contain;
          }
          .home--social__container {
            display: flex;
            flex-direction: column;
            justify-content: center;
          }
          @media screen and (max-width: 768px) {
            .home--hidden__product :global(.hidden--product) {
              overflow: hidden;
              grid-template-columns: repeat(3, 1fr);
              grid-template-rows: repeat(4, 1fr);
            }
            
            .home--top__products {
              grid-template-columns: repeat(4, 1fr);
              grid-column-gap: 5px;
            }
          }
          @media screen and (max-width: 625px) {
            .home--top__products {
              grid-template-columns: repeat(2, 1fr);
              grid-column-gap: 10px;
              grid-row-gap: 10px;
            }
          }

        `}
      </style>
    </Fragment>
  );
};
export default Home;
