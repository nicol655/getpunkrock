import React, { useContext, useEffect, useState } from "react";
import { CartContext } from "../../context/CartContext";
import { useHistory } from "react-router-dom";
import ProductCardSmall from "../../components/ProductCard/ProductCardSmall";
import Button from "../../components/Button/Button";
import { Form, Input, Checkbox, Row, Col, Steps, Modal  } from "antd";
import address_form from "../../schemas/address_form";

const { Step } = Steps;
const generate_order_endpoint = process.env.REACT_APP_GENERATE_PAYMENT_ORDER;
const validate_order_endpoint = process.env.REACT_APP_MERCADOPAGO_PAYMENT;

const Checkout = () => {
  const { cart } = useContext(CartContext);
  const [firstTouch, setFirstTouch] = useState(false);
  const [current, setCurrent] = useState(0);
  const [errorModal, showErrorModal] = useState(false);
  const [orderGenerated, setOrderGenerated] = useState({})
  let location = useHistory();
  const [form] = Form.useForm();
  let hideButton = current === 1 ? true : false;
  const steps = [
    {
      title: 'Datos de envío',
      icon: <span className="mdi mdi-package-variant"></span>,
      subtitle: current === 1 && <><span className="mdi mdi-chevron-left"></span><span>volver</span></>
    },
    {
      title: 'Pago',
      icon: current === 0 ? <span className="mdi mdi-spin mdi-loading"></span> : <span className="mdi mdi-credit-card-check-outline"></span>,
      subtitle: ''
    },
  ];

  const setPaymentForm =  () =>  {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src ='https://www.mercadopago.com.co/integrations/v1/web-tokenize-checkout.js';
    script.setAttribute('data-public-key', 'TEST-f6c0d94e-df87-43d6-8292-96736e4ce091');
    script.setAttribute('data-transaction-amount', cart.total_amount); 
    script.setAttribute('data-summary-product', cart.total_amount); 
    script.setAttribute('data-summary-product-label', 'Productos x' + cart.total_products );
    script.setAttribute('data-button-label', 'Finalizar compra'); 
    script.setAttribute('data-elements-color', '#00fa00');
    script.setAttribute('data-header-color', '#00fa00');
    document.getElementById("payment-form").appendChild(script);
  }

  const next = () => {
    setCurrent(current + 1);
  };

  const validate = () => {
    if(form.isFieldsTouched(["name", "surname", "email", "street", "address_number", "neighborhood", "postal_code"], true) || firstTouch){
      if(!form.getFieldsError().filter(({ errors }) => errors.length).length){
        let data = form.getFieldsValue(true);
        data["products"] = cart.product_list;
        fetch(generate_order_endpoint,{
          method: "POST",
          headers: {"Content-Type":"application/json"},
          body: JSON.stringify(data)
        })
        .then((res) => {
          if(res.status !== 200)
            throw new Error()
          return res.json();
        })
        .then((json) => {
          setOrderGenerated(json)
          setFirstTouch(true);
          next();
        })
        .catch(()=>{
          showErrorModal(true);
        })
        
      }
    } 
  }

  const onStepChange = (current) => {
    if(current === 0)
      setCurrent(current)
  }
  
  useEffect(() => {
    if(!cart.products.length)
      location.goBack();
  }, [cart.products.length]);

  useEffect(() =>  {
    if(current === 1)
      setPaymentForm();
  }, [current]);

  return (
    <>
      <Modal
        title={<h2>¡Vaya!</h2>}
        visible={errorModal}
        className="error--modal"
        onOk={()=>{ 
          showErrorModal(false); 
          location.push("/tienda")}}
        onCancel={()=>{
          showErrorModal(false);
          location.push("/");
        }}
        okText="Ir a la tienda"
      >
        <p><span className="mdi mdi-emoticon-dead-outline"></span> Lo sentimos, ha ocurrido un error al generar la orden de compra</p>
      </Modal>
      <div className="app--container">
        <h1 className="section--title">Información de compra</h1>
        <div className="checkout--container">
          <div className="detail--container left">
            <div className="product--detail custom--scroll">
              <h2 className="container--title">Productos</h2>
              {cart.products.length ? (
                <div className="product--list">
                  {cart.products.map((p, i) => {
                    return (
                      <ProductCardSmall
                        key={`checkout_product_card_small_${i}`}
                        cart_index={i}
                        product={{ ...p }}
                        quantity={p.quantity}
                        hideButton={hideButton}
                      />
                    );
                  })}
                </div>
              ) : ""}
            </div>
          </div>
          <div className="detail--container">
            <div className="detail--content">
              <Steps onChange={onStepChange} current={current} type="navigation">
                {steps.map(item => (
                  <Step key={item.title} title={item.title} icon={item.icon} description={item.subtitle} />
                ))}
              </Steps>
              <div className="steps-content">
                {current === 0 ? (
                  <Form form={form} layout="vertical" initialValues={{ terms: true }} className="address-form">
                    <Form.Item {...address_form["name"]}>
                      <Input {...address_form["name"].children} allowClear />
                    </Form.Item>
                    <Form.Item {...address_form["surname"]}>
                      <Input {...address_form["surname"].children} allowClear />
                    </Form.Item>
                    <Form.Item {...address_form["email"]}>
                      <Input {...address_form["email"].children} allowClear />
                    </Form.Item>
                    <Row gutter={[16, 0]}>
                      <Col span={12}>
                        <Form.Item {...address_form["street"]}>
                          <Input {...address_form["street"].children} allowClear />
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item {...address_form["address_number"]}>
                          <Input {...address_form["address_number"].children} allowClear />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={[16, 0]}>
                      <Col span={12}>
                        <Form.Item {...address_form["neighborhood"]}>
                          <Input {...address_form["neighborhood"].children} allowClear />
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item {...address_form["pcode"]}>
                          <Input {...address_form["pcode"].children} allowClear />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Form.Item name="terms" valuePropName="checked" rules={[{ required: false }]} className="conditions">
                      <Checkbox defaultChecked disabled/> 
                      <h5 className="conditions--text">Todos nuestros envios se realizan con cobro a destino</h5>
                    </Form.Item>
                    <Button
                      text="Proceder al pago"
                      icon="mdi-credit-card"
                      color="black"
                      onClick={validate}
                    />
                  </Form>
                ) : (
                  <>
                    <img src="./images/mercadopago_payment.png" alt="mercadopago metodos aceptados" className="mercadopago-pay"/>
                    <form id="payment-form" action={validate_order_endpoint} method="POST">
                      <input type="hidden" name="order_id" value={orderGenerated?.payment_order_id} />
                      <input type="hidden" name="order_status" value={orderGenerated?.status} />
                    </form>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .checkout--container {
          display: grid;
          grid-template-columns: 30vw 40vw;
          min-height: calc(100% - 80px);
          grid-column-gap: 10vw;
          margin: 0 auto;
          padding: 25px 0;
        }
        .app--container {
          justify-content: flex-start;
        }
        .mercadopago-pay {
          width: 200px;
          object-fit: contain;
          margin: 30px auto;
        }
        :global(.address-form button) {
          width: 100%;
        }
        :global(.error--modal .ant-modal-content .ant-modal-body p) {
          display: inline-flex;    
          align-items: center;
        }
        :global(.error--modal .ant-modal-content .ant-modal-body p span) {
          margin-right: 10px;
          font-size: 40px;
        }
        .steps-content {
          display: flex;
          flex-direction: column;
        }
        .mercadopago-actions-container {
          display: inline-flex;
          justify-content: space-between;
        }
        .product--detail {
          border-radius: 4px;
          border: 2px solid;
          padding: 15px;
          padding-top: 0;
        }
        .product--detail :global(.blank--state__container) {
          position: relative;
          top: 25%;
        }
        .product--list :global(.detail--price) {
          color: #212121;
          margin: 5px 0;
        }
        .product--list :global(.card--product__small) {
          border-bottom: 2px solid #f2f2f3;
        }
        .section--title {
          justify-content: flex-start;
        }
        .detail--container.left {
          display: flex;
          max-height: calc(100vh - 80px);
          overflow: hidden;
        }
        .product--detail {
          overflow-y: auto;
          width: 100%;
          display: flex;
          flex-direction: column;
        }
        .detail--content {
          margin-top: 10px;
        }
        .product--detail .container--title {
          position: sticky;
          top: 0;
          background: #fafafa;
          width: 100%;
          margin: 0;
          padding: 10px 0;
        }
        :global(.conditions .ant-form-item-control-input-content) {
          display: inline-flex;
          align-items: center;
        }
        .conditions--text {
          margin: 0 0 0 5px;
        }
        .steps-content {
          margin-top: 30px;
        }
        :global(.ant-steps-item-process > .ant-steps-item-container > .ant-steps-item-content > .ant-steps-item-title::after, .ant-steps-item-title::after) {
          background-color: #9e9e9e;
        }
        @media screen and (max-width: 768px) {
          .checkout--container {
            grid-template-columns: 1fr;
          }
        }
      `}</style>
    </>
  );
};
export default Checkout;
