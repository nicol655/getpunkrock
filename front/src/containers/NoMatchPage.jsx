import React from 'react';

const NotMatchPage = () => {
  return(
    <>
      <div className="nomatch--container app--container">
        <div className="icon--container">
          <span className="mdi mdi-emoticon-dead-outline icon"></span>
        </div>
        <div className="text">
          <h1 className="title">404</h1>
          <h2>Oops! lo sentimos</h2>
          <p>La página que buscas no existe o no está disponible</p>
        </div>
      </div>
      <style jsx>{`
        .nomatch--container {
          display: flex;
          flex-direction: row;
          align-items: center;
        }  
        .icon--container .icon {
          font-size: 10em;
          margin-right: 15px;
        }
        .title {
          font-size: 5em;
          margin-bottom: 0;
        }
        @media screen and (max-width: 425px){
          .icon--container .icon {
            font-size: 7em;
          }
          .title {
            font-size: 4em;
          }
        }
      `}</style>
    </>
  )
}
export default NotMatchPage;