import React, { useContext, useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button/Button";
import ProductCardSmall from "../../components/ProductCard/ProductCardSmall";
import { CartContext } from "../../context/CartContext";

const Cart = () => {
  const { cart, setCart } = useContext(CartContext);
  const cartRef = useRef(null);
  const product_card = useRef(null);
  let history = useHistory();
  useEffect(() => {
    if (cart.open) document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  });
  const handleClick = (e) => {
    if (
      !cartRef.current.contains(e.target) &&
      !e.target.classList.contains("detail--rm")
    ) {
      setCart({ type: "close" });
    }
  };
  return (
    <>
      <div className={`cart--container ${cart.open ? "open" : "close"}`}>
        <div ref={cartRef} className="products--container">
          <div className="cart--header">
            <h2 className="title">Tus productos</h2>
            <span
              className="mdi mdi-close cart--close"
              onClick={() => setCart({ type: "close" })}
            ></span>
          </div>

          <div className="cart--products custom--scroll cart" ref={product_card}>
            {cart.products.map((p, i) => {
              return (
                <ProductCardSmall
                  cart_index={i}
                  key={`cart_prduct_card_small_${i}`}
                  product={{ ...p }}
                  quantity={p.quantity}
                />
              );
            })}
          </div>
          <div className="cart--footer">
            <div className="footer--title">
              <h3>Total a pagar</h3>
              <h2 className="price">${cart.total_amount}</h2>
            </div>
            <div className="footer--button">
              <Button
                text="Finalizar compra"
                icon="mdi-cursor-default-click-outline"
                showText="true"
                onClick={() => {
                  setCart({ type: "close" });
                  history.push("/checkout");
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .cart--container {
          display: flex;
          z-index: -9;
          position: fixed;
          top: 0;
          width: 100%;
          left: 0;
          justify-content: flex-end;
          background: rgba(33, 33, 33, 0.7);
          height: -webkit-fill-available;
        }
        .cart--container.open {
          z-index: 99;
          opacity: 1;
          transition: opacity 300ms ease 0s;
        }
        .cart--container.open .products--container {
          transform: translateX(0px);
          transition: transform 300ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
        }
        .cart--container.close {
          opacity: 0;
          transition: opacity 300ms ease 0s;
        }
        .cart--container.close .products--container {
          transform: translateX(30px);
        }
        .products--container {
          display: flex;
          width: 100%;
          max-width: 450px;
          min-width: 320px;
          flex-direction: column;
          overflow: auto;
          background-color: #212121;
          padding: 10px 20px;
          color: #fafafa;
          box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.25);
        }
        .products--container h3, 
        .products--containerh5,
        .products--container :global(.product--name),
        .products--container .detail--price {
          margin: 0;
          color: #fafafa;
        }
        .cart--products {
          display: flex;
          flex-direction: column;
          align-items: center;
          overflow-y: auto;
          padding-right: 10px;
          margin: 20px 0;
          flex-grow: 1;
          align-items: flex-start;
        }
        .cart--header {
          display: flex;
          justify-content: space-between;
          flex: 0 0 auto;
          align-items: center;
          padding: 10px 0;
          border-bottom: 1px solid #ccc;
        }
        .cart--header .title {
          color: #fafafa;
          margin-bottom: 0;
        }
        .cart--header .cart--close {
          cursor: pointer;
          font-size: 25px;
        }
        .products--container .cart--footer {
          display: flex;
          flex-direction: column;
          flex: 0 0 auto;
          padding: 15px;
          border-top: 1px solid #ccc;
        }
        .cart--footer .footer--title {
          display: flex;
          justify-content: space-between;
          margin-bottom: 15px;
        }
        .cart--footer .footer--button {
          display: flex;
          flex-grow: 1;
          justify-content: center;
        }
        .cart--footer .footer--button :global(button) {
          width: 100%;
          font-size: 18px;
        }
        .footer--title {
          align-items: center;
        }
        .cart--products :global(.card--product__small:last-child) {
          border-bottom: none;
        }
        .cart--products :global(.card--product__small .product--detail .product--size), .footer--title .price {
          color: #fafafa;
        }
      `}</style>
    </>
  );
};
export default Cart;
