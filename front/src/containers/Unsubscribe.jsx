import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const Unsubscribe = () => {
  const [statusCode, setStatusCode] = useState();
  let { hash } = useParams();
  const subscribe_endpoint = process.env.REACT_APP_SUBSCRIBE_ENDPOINT;
  useEffect(() => {
    fetch(`${subscribe_endpoint}${hash}`, {
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "GET",
    }).then((res) => {
      res.json()
        .then(() => {
          setStatusCode(res.status);
        })
    });
  },[subscribe_endpoint, hash])
  return(
    <>
      <div className="app--container">
        <div className="content">
          {statusCode === 403 && <h1><span className="mdi mdi-bell-remove"></span>Parece que ya te desuscribiste o este link no esta disponible.</h1> }
          {statusCode === 200 && (
            <h1>
              <span className="mdi mdi-heart-broken-outline message--icon"></span>
              Lamentamos que te tengas que ir.
            </h1>
          )}
          {statusCode === 400 &&
            <h1><span className="mdi mdi-google-downasaur message--icon"></span>Lo sentimos, ha ocurrido un error.</h1>
          }
        </div>
      </div>
      <style jsx>{`
        .content {
          display: flex;
          flex-direction: column;
          flex-grow: 1;
          justify-content: center;
          align-items: center;
          text-align: center;
        }  
        .message--icon {
          color: #212121;
        }
      `}
      </style>
    </>
  )
}
export default Unsubscribe;