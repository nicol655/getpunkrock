import React from "react";
import { Form, Select, Row, Col } from 'antd';
import { Input } from 'antd';

import Button from "../components/Button/Button";

const Contact = () => {
  const { Option } = Select;
  const { TextArea } = Input;

  const handleChange = (value) => {
    console.log(value)
  }
  const onChange = e => {
    console.log(e);
  };
  return (
    <>
      <div className="app--container contact--container">
        <h1 className="section--title">Comunícate con nosotros</h1>
        <div className="contact--content">
          <Form>
            <Form.Item>
              <Select defaultValue="0"  onChange={handleChange}>
                <Option value="0">Hacer una consulta</Option>
                <Option value="1">Encargar mercancia que esta fuera de stock</Option>
                <Option value="2">Quiero vender mi marca con ustedes</Option>
              </Select>
            </Form.Item>
            <Row gutter={[16, 0]}>
              <Col span={12}>
                <Form.Item  rules={[{ required: true }]}>
                  <Input placeholder="Nombre completo" allowClear onChange={onChange} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={[16, 0]}>
              <Col span={12}>
                <Form.Item rules={[{ required: true }]}>
                  <Input placeholder="micorreo@prueba.com" allowClear onChange={onChange} />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
              <TextArea placeholder="" allowClear onChange={onChange} />
            </Form.Item>
            <Row>
              <Col span={12} offset={6}>
                <Button
                  text="Enviar"
                  customClass="contact"
                  icon="mdi-email"
                  showText="true"
                />
              </Col>
            </Row>
          </Form>
        </div>
      </div>
      <style jsx>{`
        .contact--container {
          align-items: center;
          display: flex;
          justify-content: center;
          min-width: 80vw;
        }
        .section--title {
          align-self: flex-start;
          margin-bottom: 30px;
          width: 100%;
        }
        .contact--content {
          padding: 20px 60px;
          display: flex;
          width: 100%;
          flex-direction: column;
          background: #eaecee;
        }
      
        .info--container :global(.input--container) {
          flex: 1 1 100%;
        }
        .info--container :global(.input--container:first-child) {
          margin-right: 80px;
        }
        textarea {
          width: -webkit-fill-available;
          height: 20vh;
          padding: 10px;
          background: transparent;
          border: 1px solid #cccccc;
        }
        textarea:focus {
          border: 1px solid #00fa00;
          box-shadow: 0 0 0 2px rgba(103, 255, 24, 0.2);
          outline: none;
        }
        :global(button.contact) {
          color: #212121;
        }
        :global(button.contact::before) {
          border-top-color: #212121;
          border-left-color: #212121;
        }
        :global(button.contact::after) {
          border-bottom-color: #212121;
          border-right-color: #212121;
        }
        :global(button.contact) {
          width: 100%;
          align-self: center;
        }
        :global(.custom--antd.ant-select) {
          width: 100%;
        }
        .ant-input-affix-wrapper:focus, .ant-input-affix-wrapper-focused {
          box-shadow: 0 0 0 2px rgba(103, 255, 24, 0.2);
        }
        @media screen and (max-width: 768px) {
          .info--container {
            flex-wrap: wrap;
          }
          .info--container :global(.input--container:first-child) {
            margin: 0;
          }
        }
        @media screen and (max-width: 495px) {
          .contact--content {
            padding: 20px;
          }
        }
      `}</style>
    </>
  );
};
export default Contact;
