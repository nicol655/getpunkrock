import React from "react";
import { Radio } from 'antd';

const SizeChart = ({ onChange, subProducts}) => {
 
  const handleChangeSizeChart = (e) => {
    onChange(e.target.value);
  }
  return (
    <>
      <div className="chart--container">
          <Radio.Group className="custom--antd" onChange={handleChangeSizeChart} defaultValue={0} size="small">
            {subProducts.map((item, index)=>{
              return(
                <Radio.Button key={`size-chart-opt-${index}`} value={index}>{item.sizeKey}</Radio.Button>
              )
            })}
          </Radio.Group>
      </div>
      <style jsx>{`
        .chart--container {
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
        }
        .chart--container button {
          border: 1px solid #212121;
          padding: 5px 10px;
          width: 40px;
          height: 30px;
          cursor: pointer;
          text-transform: uppercase;
          font-size: 10px;
          font-family: nunito-bold;
        }
        .chart--container button {
          background: #212121;
          color: #fafafa;
        }
        .chart--container button:not(:first-child) {
          border-left: 0;
        }
        :global(.ant-radio-button-wrapper) {
          font-size: 11px;
        }
      `}</style>
    </>
  );
};
export default SizeChart;
