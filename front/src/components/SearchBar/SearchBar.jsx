import React, { useState  } from "react";
import { Select } from 'antd';

const SearchBar = ({placeholder, className}) => {
  const [searchState, setSearchState] = useState({data: [], value: undefined, fetching: false});

  const { Option } = Select;

  const handleChange = value => {
    setSearchState({ ...searchState, data: [], value, fetching: false});
  }
  const placeholderSearchIcon = (placeholder) =>{
    return(
      <>
        <span className="mdi mdi-magnify"></span> {placeholder}
      </>
    );
  }
  return (
    <>
      <Select
        className={className}
        allowClear
        showSearch
        value={searchState.value}
        placeholder={placeholderSearchIcon(placeholder)}
        defaultActiveFirstOption={false}
        showArrow={false}
        filterOption={false}
        onSearch={()=>{}}
        onChange={handleChange}
        notFoundContent={null}
      >
        <Option key="test">Resultado de prueba</Option>
      </Select>
      <style jsx>{`
        .searchbar--container {
          display: flex;
          flex-direction: row;
          align-items: center;
        }
      `}</style>
    </>
  );
};
export default SearchBar;
