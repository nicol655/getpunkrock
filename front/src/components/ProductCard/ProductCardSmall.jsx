import React from "react";
import { CartContext } from "../../context/CartContext";
import { useContext } from "react";

const ProductCardSmall = ({ product, quantity, cart_index, hideButton=false }) => {
  const { setCart } = useContext(CartContext);
  const back_host = process.env.REACT_APP_BACKEND_ENDPOINT;
  let selectedSubproduct = product.subProducts[product.selectedSubProduct];
  return (
    <>
      <div className="card--product__small">
        <div className="product--image">
          <img src={back_host + product.images[0].image_url} alt="producto en la lista de compras" />
        </div>
        <div className="product--detail">
          <h4 className="product--name">{product.name} - {product.code}</h4>
          <h5 className="product--size">Talla: {selectedSubproduct.sizeKey}</h5>
          <h2 className="detail--price">${selectedSubproduct.price}</h2>
          {!hideButton && (
            <button
              className="detail--rm"
              onClick={() => {
                setCart({ type: "remove", index: cart_index });
              }}
            >
              eliminar
            </button>
          )}
          
        </div>
        <div className="product--quantity">
          <span>
            <span>{quantity}</span>
          </span>
        </div>
      </div>
      <style jsx>
        {`
          .card--product__small {
            display: flex;
            width: 100%;
            align-items: center;
            padding: 5px 0;
            border-bottom: 1px solid #cccccc2b;
          }
          .product--quantity {
            display: flex;
            height: 25px;
            min-width: 25px;
            padding: 5px;
            align-items: center;
            border-style: none;
            border-radius: 4px;
            background-color: #9e9e9e52;
            position: relative;
            justify-content: center;
            font-size: 12px;
          }
          .product--image img {
            width: 90px;
            object-fit: contain;
          }
          .product--detail {
            display: flex;
            flex-direction: column;
            margin: 0 15px;
            flex-grow: 1;
          }
          .product--size, .product--name {
            margin: 0;
          }
          .detail--price {
            color: #fafafa;
            margin: 0;
            font-family: nunito;
            font-size: 16px;
            font-weight: bold;
          }
          .detail--rm {
            text-transform: capitalize;
            cursor: pointer;
            color: #00fa00;
            display: flex;
            padding: 0;
            align-self: end;
          }
        `}
      </style>
    </>
  );
};
export default ProductCardSmall;
