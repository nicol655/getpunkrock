import React, { Fragment, useState } from "react";
import { useHistory } from "react-router-dom";

import ProductModal from "../ProductModal";

const ProductCard = ({ product }) => {
  const [productModal, setProductModal] = useState({visible: false, actions: {first: "", second: ""}})
  const back_host = process.env.REACT_APP_BACKEND_ENDPOINT;
  let history = useHistory();
  const handleModal = () => {
    setProductModal((prevState) => {
      return { ...prevState, visible: !productModal.visible, actions: {first: () => openCheckout()}};
    });
  };
  const openSingleProduct = () => {
    let url = `/producto/${product.id}/${product.name}/${product.code}/`;
    history.push(url);
  };
  const openCheckout =()=>{
    let url = `/checkout`;
    history.push(url);
  }
  return (
    <Fragment>
      {productModal.visible && (
        <ProductModal visible={productModal.visible} handleModal={handleModal} product={product} actions={productModal.actions}/>
      )}
      <div className="card--container">
        <div className="card--content__img">
          <span
            className="zoom--image mdi mdi-information-outline"
            onClick={openSingleProduct}
          ></span>
          <img src={back_host + product.images[0]?.image_url} alt="product novedad" />
          <div className="overlay" />
        </div>
        <div className="card--content__desc">
          <h4 className="product--title" onClick={openSingleProduct}>
            {product.name} - {product.code}
          </h4>
          <h2 className="product--price">${product.subProducts[0]?.price}</h2>

          <div className="add--product">
            <button className="product--detail" onClick={openSingleProduct}>
              <span className="mdi mdi-information-outline"></span>
            </button>
            <button className="product--detail" onClick={handleModal}>
              <span className="mdi mdi-cart-plus"></span>
            </button>
          </div>
        </div>
      </div>
      <style jsx>{`
        .add--product {
          display: flex;
          justify-content: space-between;
          font-size: 20px;
        }
        .card--container {
          border-radius: 4px;
          border: 1px solid #80808036;
          overflow: hidden;
          display: flex;
          flex-direction: column;
        }
        .card--content__desc {
          padding: 10px;
          display: flex;
          flex-direction: column;
          background: #fff;
          flex-grow: 1;
          color: #212121;
        }
        .product--price {
          padding-bottom: 5px;
          border-bottom: 2px solid #212121;
          font-weight: 400;
        }
        .product--title {
          display: flex;
          align-items: flex-start;
          flex-grow: 1;
          margin: 0 0 10px 0;
          font-family: nunito;
        }
        .add--product {
          display: flex;
          align-items: center;
          margin-top: 5px;
        }
        .add--product select {
          margin-right: 15px;
          width: 45px;
          height: 22px;
          border: 1px solid #212121;
        }
        .product--detail {
          padding: 0;
          color: #212121;
          font-weight: bold;
          align-self: flex-end;
        }
        .product--detail.mdi {
          font-size: 21px;
        }
        .product--detail:hover {
          color: #4CAF50;
        }
        .product--detail,
        .add--product select,
        .product--title {
          cursor: pointer;
        }
        .card--container {
          flex-grow: 1;
        }
        .card--content__img {
          display: flex;
          justify-content: center;
          position: relative;
          overflow: hidden;
        }
        .card--content__img img {
          transition: transform 0.5s ease;
          width: 100%;
          height: 100%;
        }
        .card--content__img:hover .zoom--image {
          visibility: visible;
          z-index: 1;
          transition: visibility 0.5s ease;
        }
        .card--content__img:hover .overlay {
          visibility: visible;
          background-color: rgba(0, 0, 0, 0.6);
        }
        .zoom--image {
          position: absolute;
          font-size: 30px;
          align-self: center;
          visibility: hidden;
          color: white;
          z-index: -1;
        }
        .zoom--image:hover {
          cursor: pointer;
        }
        .zoom--image:hover + img {
          transform: scale(1.5);
        }
      `}</style>
    </Fragment>
  );
};
export default ProductCard;
