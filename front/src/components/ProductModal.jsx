import React, { useContext, useState } from "react";
import { Modal, Button } from 'antd';
import { InputNumber } from 'antd';

import { CartContext } from "../context/CartContext";
import SizeChart from "../components/SizeChart";

const ProductModal = ({visible, handleModal, actions, product}) => {
  const [modalProductAdded, setmodalProductAdded] = useState({message: "Agregar"});
  const [selectedSize, setSelectedSize] = useState(0);
  const { setCart } = useContext(CartContext);
  const [productQuantity, setProductQuantity] = useState(1);
  const back_host = process.env.REACT_APP_BACKEND_ENDPOINT;

  const handleChange = (value) => {
    setProductQuantity(value);
  };
  const addProduct = () => {
    setmodalProductAdded({...modalProductAdded, message:"Agregado!"})
    setCart({
      type: "add",
      product: { ...product, quantity: productQuantity, selectedSubProduct: selectedSize },
    });
    setTimeout(()=>{
      setmodalProductAdded({...modalProductAdded, message:"Agregar"})
      handleModal();
    },1000)
  }

  return(
    <>
      <Modal
        visible={visible}
        onCancel={handleModal}
        footer={[
          <Button key="back" className="button--confirm--transparent" onClick={addProduct}>
            {modalProductAdded.message}
          </Button>,
          <Button key="submit" className="button--confirm" type="primary" onClick={actions.first}>
            Finalizar compra
          </Button>,
        ]}
      >
        <div className="product--primary">
          <h1 className="title">{product.name} - {product.code}</h1>
          <img className="image" src={back_host + product.images[0].image_url} alt="product primary"/>
          <div className="description">
            <h2 className="price">${product.subProducts[selectedSize].price}</h2>
            <p>
              {product.description}
            </p>
            <div className="product--size">
              <h5 className="bold">Tallas</h5>
              <SizeChart subProducts={product.subProducts} onChange={setSelectedSize}/>
              <button className="product--size__chart bold">
                <span className="mdi mdi-ruler icon"></span> <span>Tabla de medidas</span> 
              </button>
            </div>
            <div className="add--product">
            <h5 className="bold">Agregar al carro</h5>
            <div className="product--input">
              <InputNumber className="custom--antd" min={1} max={product.subProducts[0].stock} defaultValue={1} onChange={handleChange} />
              {product?.subProducts && 
                product?.subProducts[0].stock < 20 && 
                product?.subProducts[0].stock > 20 ?  
                  <span className="warning--span">Quedan {product?.subProducts[0].stock} en stock!</span>
                : "" }
            </div>
          </div>
          </div>
        </div>
        
      </Modal>
     <style jsx>
       {`
          img {
            height: 200px;
            object-fit: contain;
          }
          .product--primary {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
          }
          .product--primary .title{
            flex-grow: 1;
          }
          .product--primary .image, .product--primary .description {
            flex: 1 1 50%;
          }
          .product--size__chart {
            font-size: 12px;
            color: #4CAF50;
            cursor: pointer;
          }
          .product--size__chart .icon{
            font-size: 16px;
          }
          .product--input input {
            width: 40px;
            border: 1px solid;
            margin-right: 5px;
          }
          .product--size {
            margin-bottom: 5px;
          }
          
       `}
       </style> 
    </>
  )
}
export default ProductModal;