import React, { useState } from "react";

const Accordion = ({ title, children }) => {
  const [open, setState] = useState(false);
  return (
    <>
      <div
        className={`accordion--element ${open ? "open" : "closed"}`}
        onClick={() => {
          setState(!open);
        }}
      >
        <div className="title--container">
          <h4>{title}</h4>
          <span className={`mdi ${open ? "mdi-minus" : "mdi-plus"} `}></span>
        </div>
        <div className="accordion--content">{children}</div>
      </div>
      <style jsx>{`
        .accordion--element {
          border-bottom: 2px solid #00fa00;
          cursor: pointer;
        }
        .accordion--element h4 {
          margin: 10px 0;
        }
        .accordion--element.closed .accordion--content {
          margin-bottom: 0px;
          max-height: 0;
          transition: max-height 0.35s cubic-bezier(0, 1, 0, 1),
            margin-bottom 0.3s ease;
        }
        .accordion--element .accordion--content {
          margin: 0;
          overflow: hidden;
          transition: max-height 0.3s cubic-bezier(1, 0, 1, 0),
            margin-bottom 0.3s ease;
          height: auto;
          max-height: 9999px;
          margin-bottom: 15px;
        }
        .title--container {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }
        .title--container .mdi {
          font-size: 15px;
        }
      `}</style>
    </>
  );
};
export default Accordion;
