import React, { Fragment } from "react";
import ImageGallery from "react-image-gallery";

const Slider = ({ children, ...rest }) => {
  return (
    <Fragment>
      <ImageGallery {...rest}>{children}</ImageGallery>
      <style jsx global>
        {`
          .image-gallery-fullscreen-button .image-gallery-svg {
            height: 25px;
            width: 25px;
          }
          .image-gallery-thumbnail.active, .image-gallery-thumbnail:hover, .image-gallery-thumbnail:focus {
            border-color: #00fa00;
          }
        `}
      </style>
    </Fragment>
  );
};
export default Slider;
