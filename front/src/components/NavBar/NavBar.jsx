import React, { useState, useContext, useRef } from "react";
import { useEffect } from "react";
import { NavLink } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import { CartContext } from "../../context/CartContext";

const NavBar = ({ fixture, hideCart }) => {
  const [menuState, setMenuState] = useState(false);
  const { cart, setCart } = useContext(CartContext);
  const [scrollPos, setScrollPos] = useState(0);
  const [menuVisible, setMenuVisible] = useState(true);
  const menuRef = useRef(null);
  const handleMenuState = (e) => {
    if (!menuRef.current.contains(e.target)) setMenuState(false);
  };

  useEffect(() => {
    document.addEventListener("click", (e) => handleMenuState(e));
    return document.removeEventListener("click", (e) => handleMenuState(e));
  }, []);

  useEffect(() => {
    const screen = document.querySelector("body");
    if(screen) {
      screen.addEventListener("scroll", () => {
        let currentScrollPos = screen.scrollTop;
        setMenuVisible(scrollPos > currentScrollPos || currentScrollPos < 10);
        setScrollPos(currentScrollPos);
      });
      return () => screen.removeEventListener("scroll", ()=>{});
    }
  }, [scrollPos, menuVisible]);

  return (
    <>
      <div
        className={`nav--container ${menuState ? "menu-open" : "menu-close"} ${
          menuVisible ? "menu-visible" : "menu-hidden"
        }`}
        ref={menuRef}
      >
        <div className="nav--content">
          <button
            className={`mobile--button tablet`}
            onClick={() => setMenuState(!menuState)}
          ></button>
          <nav className={`nav ${menuState ? "open" : "close"}`}>
            <div className="page--nav">
              {fixture.options.map((option, i) => {
                return (
                  <NavLink
                    key={`option-nav-${i}`}
                    className="nav-item"
                    to={option.pathname}
                    isActive={(_, location) =>
                      location.pathname === option.pathname
                    }
                    onClick={() => setMenuState(false)}
                  >
                    {option.name}
                  </NavLink>
                );
              })}
            </div>
            <div className="social--nav">
              <a
                className="social--link"
                href="https://www.instagram.com/getpunkrock/"
                rel="noreferrer noopener"
                target="_blank"
              >
                <span className="mdi mdi-instagram"></span>
              </a>
              <a
                className="social--link"
                href="https://www.facebook.com/GetPunkRock"
                rel="noreferrer noopener"
                target="_blank"
              >
                <span className="mdi mdi-facebook"></span>
              </a>
              <a
                className="social--link"
                href="https://wa.link/h00q0f"
                rel="noreferrer noopener"
                target="_blank"
              >
                <span className="mdi mdi-whatsapp"></span>
              </a>
            </div>
          </nav>
          {!hideCart && (
            <button
              className="cart--button"
              onClick={() => setCart({ type: "open" })}
            >
              <span className="mdi mdi-cart"></span>
              <span className="cart--products__count">{cart.total_products}</span>
            </button>
          )}
          <div className="logo--container">
            <div className="left--logo">
              <img src={Logo} className="logo" alt="Logo"></img>
            </div>
            <div className="right--logo">
              <img src={Logo} className="logo" alt="Logo"></img>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .nav--container {
          right: 0;
          position: fixed;
          width: 100vw;
          background-color: #212121;
          overflow: hidden;
          z-index: 9;
          display: flex;
          justify-content: space-between;
        }
        .nav--content {
          max-width: 1600px;
          display: flex;
          flex-grow: 1;
          margin: 0 auto;
        }
        .menu-visible {
          top: 0;
          transition: all 0.3s ease-in;
        }
        .menu-hidden {
          top: -70px;
          transition: all 0.3s ease-out;
        }
        .menu-open .mobile--button {
          box-shadow: 0 0 0 100vw rgba(0, 0, 0, 0.1),
            inset 0 0 0 20px rgba(0, 0, 0, 0.1);
        }
        .menu-open .mobile--button:before {
          transform: translateY(-50%) rotate(45deg) scale(1);
        }
        .menu-open .mobile--button:after {
          transform: translateY(-50%) rotate(-45deg) scale(1);
        }
        nav.open {
          margin-bottom: 100px;
          pointer-events: auto;
          transform: translate(50px, 50px);
        }
        .logo--container {
          display: flex;
          height: 67px;
          margin-right: 35px;
          align-items: center;
          position: relative;
        }
        .right--logo,
        .left--logo {
          transform: translateY(0px) rotate3d(1, 1, 10, 0deg);
          transition: transform 1s ease;
        }
        .logo--container:hover {
          cursor: pointer;
        }
        .logo--container:hover .right--logo {
          transform: translateY(20px) rotate3d(1, 1, 10, 17deg);
          transition: transform 1s ease;
        }
        .logo--container:hover .left--logo {
          transform: translateY(20px) rotate3d(1, 1, 10, -17deg);
          transition: transform 1s ease;
        }

        .left--logo,
        .right--logo {
          object-fit: contain;
          display: flex;
          overflow: hidden;
        }
        .left--logo {
          height: 60px;
          width: 38px;
        }
        .left--logo img {
          width: 100%;
          object-fit: cover;
          object-position: 0;
        }

        .right--logo {
          width: 90px;
          height: 60px;
        }
        .right--logo img {
          object-position: -38px;
          object-fit: cover;
          width: 100%;
        }

        .cart--button {
          margin-left: auto;
          color: #00fa00;
          align-self: flex-start;
          margin-top: 20px;
          font-size: 23px;
          position: relative;
          z-index: 12;
          cursor: pointer;
          margin-right: 10px;
        }
        .cart--products__count {
          position: absolute;
          right: 3px;
          bottom: 0;
          font-size: 9px;
          background: #fafafa;
          border-radius: 50%;
          height: 15px;
          width: 15px;
          display: flex;
          align-items: center;
          justify-content: center;
          color: #212121;
        }

        .menu-close .mobile--button:hover {
          box-shadow: 0 0 0 8px rgba(0, 0, 0, 0.1),
            inset 0 0 0 20px rgba(0, 0, 0, 0.1);
        }
        .mobile--button:before,
        .mobile--button:after {
          position: absolute;
          content: "";
          top: 50%;
          left: 0;
          width: 100%;
          height: 2px;
          background-color: #00fa00;
          border-radius: 5px;
          transition: 0.5s;
        }
        .menu-close .mobile--button:before {
          transform: translateY(-50%) rotate(45deg) scale(0);
        }
        .menu-close .mobile--button:after {
          transform: translateY(50%) rotate(-45deg) scale(0);
        }
        * {
          box-sizing: border-box;
        }
        .nav {
          display: flex;
          flex-grow: 1;
          justify-content: space-between;
          transition: 0.5s;
        }
        .nav.open .social--nav {
          height: 100%;
          transition: height 0.5s ease-in;
        }
        .social--nav {
          display: flex;
          overflow: hidden;
          flex-direction: column;
          justify-content: space-around;
          transition: height 0.2s ease-out;
          height: 0;
        }
        .social--nav .social--link {
          color: #00fa00;
          font-size: 20px;
        }
        :global(.nav-item) {
          font-size: 16px;
          color: #fafafa;
          text-transform: capitalize;
          padding: 0 15px;
          height: 35px;
          line-height: 35px;
          text-decoration: none;
          position: relative;
          display: inline-flex;
          margin: 15px 10px;
          outline: none;
        }
        :global(.nav-item:hover, .nav-item.active) {
          color: #00fa00;
        }
        :global(.nav-item:before),
        :global(.nav-item:after) {
          position: absolute;
          width: 35px;
          height: 2px;
          content: "";
          opacity: 1;
          -webkit-transition: all 0.3s;
          -moz-transition: all 0.3s;
          transition: all 0.3s;
        }
        :global(.nav-item:before) {
          top: 0;
          left: 0;
          transform: rotate(90deg);
          transform-origin: 0 0;
        }
        :global(.nav-item:after) {
          right: 0;
          bottom: 0;
          transform: rotate(90deg);
          transform-origin: 100% 2px;
        }
        :global(.nav-item:hover:before, .nav-item.active:before) {
          left: 50%;
          transform: rotate(0deg) translateX(-50%);
          opacity: 1;
          background: #00fa00;
        }
        :global(.nav-item:hover:after, .nav-item.active:after) {
          right: 50%;
          transform: rotate(0deg) translateX(50%);
          opacity: 1;
          background: #00fa00;
        }
        @media screen and (max-width: 768px) {
          .mobile--button {
            position: absolute;
            display: inline-block;
            width: 20px;
            height: 30px;
            margin: 25px;
            background-color: transparent;
            border: none;
            cursor: pointer;
            border-radius: 100%;
            transition: 0.6s;
          }
          .cart--button {
            position: absolute;
            right: 152px;
          }
          .logo--container {
            position: absolute;
            right: -10px;
          }
          .nav,
          :global(.nav-item) {
            pointer-events: none;
          }
          .nav--content {
            min-height: 67px;
          }
          .nav {
            margin: 25px 25px 20px;
          }
          nav.open {
            transform: translate(0px,50px);
          }
          nav.open :global(.nav-item) {
            color: #00fa00;
            letter-spacing: 0;
            height: 40px;
            line-height: 40px;
            margin-top: 0;
            opacity: 1;
            transform: scaleY(1);
            transition: 0.5s, opacity 0.1s;
          }
          nav.open :global(.nav-item:nth-child(1)) {
            transition-delay: 0.15s;
          }
          nav.open :global(.nav-item:nth-child(1)):before {
            transition-delay: 0.15s;
          }
          nav.open :global(.nav-item:nth-child(2)) {
            transition-delay: 0.1s;
          }
          nav.open :global(.nav-item:nth-child(2)):before {
            transition-delay: 0.1s;
          }
          nav.open :global(.nav-item:nth-child(3)) {
            transition-delay: 0.05s;
          }
          nav.open :global(.nav-item:nth-child(3)):before {
            transition-delay: 0.05s;
          }
          nav.open :global(.nav-item:nth-child(4)) {
            transition-delay: 0s;
          }
          nav.open :global(.nav-item:nth-child(4)):before {
            transition-delay: 0s;
          }
          nav.open :global(.nav-item):before {
            opacity: 0;
          }
          :global(.nav-item) {
            text-decoration: none;
            position: relative;
            display: inline-block;
            float: left;
            clear: both;
            margin: 0;
            padding: 0;
            color: transparent;
            font-size: 14px;
            letter-spacing: -6.2px;
            height: 7px;
            line-height: 7px;
            text-transform: uppercase;
            white-space: nowrap;
            transform: scaleY(0.2);
            transition: 0.5s, opacity 1s;
          }
          .nav.open :global(.nav-item) {
            pointer-events: auto;
          }
          :global(.nav-item:nth-child(1)) {
            transition-delay: 0s;
          }
          :global(.nav-item:nth-child(1)):before {
            transition-delay: 0s;
          }
          :global(.nav-item:nth-child(2)) {
            transition-delay: 0.05s;
          }
          :global(.nav-item:nth-child(2)):before {
            transition-delay: 0.05s;
          }
          :global(.nav-item:nth-child(3)) {
            transition-delay: 0.1s;
          }
          :global(.nav-item:nth-child(3)):before {
            transition-delay: 0.1s;
          }
          :global(.nav-item:nth-child(4)) {
            transition-delay: 0.15s;
          }
          :global(.nav-item:nth-child(4)):before {
            transition-delay: 0.15s;
          }
          :global(.nav-item:nth-child(1)) {
            letter-spacing: -4px;
          }
          :global(.nav-item:nth-child(2)) {
            letter-spacing: -7px;
          }
          :global(.nav-item:nth-child(3)) {
            letter-spacing: -7px;
          }
          :global(.nav-item:nth-child(n + 4)) {
            letter-spacing: -8px;
            margin-top: -7px;
            opacity: 0;
          }
          :global(.nav-item:before, .nav-item.active:before) {
            position: absolute;
            content: "";
            top: 50%;
            left: 0;
            width: 100%;
            height: 2px;
            background-color: #00fa00;
            transform: translateY(-50%) scaleY(5);
            transition: 0.5s;
          }
          :global(.nav-item:after, .nav-item.active:after, .nav-item.active:focus:after) {
            content: "";
            height: 2px;
            background-color: #00fa00;
            width: 0px;
            right: 0;
            display: flex;
            transform: skew(0, -10deg) translate(0px, -23px);
            opacity: 0;
          }
          
          :global(.nav-item.active) {
            color: transparent;
          }
          .nav.open :global(.nav-item.active:after) {
            width: 100%;
            opacity: 1;
          }
          :global(.nav-item:hover:after) {
            width: 100%;
            opacity: 1;
            right: 0;
            transform: skew(0, -10deg) translate(0px, -23px);
            transition: width 0.5s ease;
          }
        }
      `}</style>
    </>
  );
};
export default NavBar;
