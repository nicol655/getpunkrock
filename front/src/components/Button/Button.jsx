import React, { Fragment } from "react";

const Button = ({ text, onClick, icon, showText, customClass, color, ...rest }) => {
  let button_classes = `rectangle--style ${customClass ? customClass : ""} ${
    color ? color : ""
  }`;
  return (
    <Fragment>
      <button className={button_classes} onClick={onClick} {...rest}>
        <span className="circle" aria-hidden="true">
          {icon ? (
            <span className={`mdi ${icon}`}></span>
          ) : (
            <span className="icon arrow"></span>
          )}
        </span>
        <span className={`${showText ? "button-text" : "button-text hidden"}`}>
          {text}
        </span>
      </button>
      <style jsx>{`
        button {
          position: relative;
          padding: 12px 15px;
          border: none;
          background-color: transparent;
          cursor: pointer;
          outline: none;
          font-size: 14px;
        }
        .rectangle--style {
          color: #00fa00;
        }
        .rectangle--style::after,
        .rectangle--style::before {
          content: "";
          display: block;
          position: absolute;
          width: 20%;
          height: 20%;
          border: 2px solid;
          transition: all 0.6s ease;
          border-radius: 2px;
        }
        .rectangle--style::after {
          bottom: 0;
          right: 0;
          border-top-color: transparent;
          border-left-color: transparent;
          border-bottom-color: #00fa00;
          border-right-color: #00fa00;
        }
        .rectangle--style::before {
          top: 0;
          left: 0;
          border-bottom-color: transparent;
          border-right-color: transparent;
          border-top-color: #00fa00;
          border-left-color: #00fa00;
        }
        .rectangle--style:hover:after,
        .rectangle--style:hover:before {
          border-bottom-color: #00fa00;
          border-right-color: #00fa00;
          border-top-color: #00fa00;
          border-left-color: #00fa00;
          width: 100%;
          height: 100%;
        }
        .black {
          color: #212121;
        }
        .black::after {
          border-bottom-color: #212121;
          border-right-color: #212121;
        }
        .black::before {
          border-top-color: #212121;
          border-left-color: #212121;
        }
        .black:hover:after,
        .black:hover:before {
          border-bottom-color: #212121;
          border-right-color: #212121;
          border-top-color: #212121;
          border-left-color: #212121;
        }
      `}</style>
    </Fragment>
  );
};
export default Button;
