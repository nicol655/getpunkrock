import React from "react";
import Button from "../components/Button/Button";

const BlankState = ({ title, description, buttonText, buttonAction, icon }) => {
  return (
    <>
      <div className="blank--state__container">
        <h1 className="blank--title">{title}</h1>
        <p className="blank--description">{description}</p>
        {buttonText && <Button text={buttonText} onClick={buttonAction} />}
        {icon && <span className={`mdi ${icon} icon`}></span>}
      </div>
      <style jsx>{`
        .blank--state__container {
          display: flex;
          flex-direction: column;
          align-items: center;
          border: 2px dashed #00fa00;
          background-color: #212121;
          color: #fafafa;
          padding: 10px;
        }
        .blank--title {
          color: #fafafa;
        }
        .blank--description {
          margin: 0;
        }
        .icon {
          font-size: 6em;
        }
      `}</style>
    </>
  );
};
export default BlankState;
