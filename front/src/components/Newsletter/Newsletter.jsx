import React, { Fragment } from "react";
import Button from "../Button/Button";
import { Input, Form, Row, Col, notification  } from 'antd';

const Newsletter = () => {
  const [form] = Form.useForm();
  const subscribe_endpoint = process.env.REACT_APP_SUBSCRIBE_ENDPOINT;

  const onSubscribe = () => {
    fetch(subscribe_endpoint,{
      method: "POST",
      body: JSON.stringify(form.getFieldsValue()),
      headers: { "Content-Type": "application/json" }
    })
    .then((response)=> {
      if(!response.ok)  
        throw new Error();
      openNotification('¡Gracias por suscribirte!',
      'Te damos la bienvenida a nuestra lista de correos',
      'mdi-emoticon-kiss-outline')
    })
    .catch(()=> openNotification('¡Vaya!',
    'La cuenta de email que intentas usar ya esta suscrita, intenta con una diferente.',
    'mdi-emoticon-neutral-outline'))
  }
  const openNotification = (message, description, icon) => {
    notification.open({
      message: message,
      description:
      description,
      icon: <span className={`mdi ${icon}`}></span>,
    });
  };
  return (
    <Fragment>
      <div className="newsletter--container">
          <div className="newsletter--content">
            <h3 className="newsletter--text">
              <span className="mdi mdi-cursor-default-click"></span>
              Suscribete a nuestro boletín para recibir novedades, ofertas y
              descuentos
            </h3>
            <Form form={form} onFinish={onSubscribe}>
                <Row gutter={[16, 0]}>
                  <Col span={24} md={12}>
                    <Form.Item name="first_name" rules={[{ required: true, message: 'Por favor, ingrese un nombre válido!', pattern: /^[a-zA-ZñÑÀ-ú]+$/ }]}>
                      <Input placeholder="Nombre" allowClear />
                    </Form.Item>
                  </Col>
                  <Col span={24} md={12}>
                    <Form.Item name="last_name" rules={[{ required: true, message: 'Por favor, ingrese un apellido válido!', pattern: /^[a-zA-ZñÑÀ-ú]+$/ }]}>
                      <Input placeholder="Apellido" allowClear />
                    </Form.Item>
                  </Col>
                </Row>
              
              <Form.Item name="email" rules={[{ required: true, message: 'Por favor, ingrese un correo válido!', type:"email" }]}>
                <Input placeholder="micorreo@prueba.com" allowClear />
              </Form.Item>
            </Form>
            <Button text="suscribirme" icon="mdi-bell-ring" showText="true" onClick={form.submit}/>
          </div>
      </div>
      <style jsx>{`
        .newsletter--container {
          display: flex;
          height: auto;
          width: 100%;
          background: #212121;
          flex-direction: column;
          color: #fafafa;
          justify-content: center;
          max-width: 1600px;
          min-height: 250px;
          margin: 20px 0;
          align-items: center;
        }
        .newsletter--content {
          flex-direction: column;
          max-width: 1000px;
          padding: 0 25px;
          padding: 25px;
          display: flex;
          align-items: flex-start;
          flex-grow: 1;
          justify-content: space-between;
        }
        .newsletter--text {
          font-weight: lighter;
          color: #00fa00;
          text-transform: uppercase;
          margin: 0;
          display: flex;
        }
        :global(.ant-form-item-has-error .ant-input, 
        .ant-form-item-has-error .ant-input-affix-wrapper, 
        .ant-form-item-has-error .ant-input:hover, 
        .ant-form-item-has-error .ant-input-affix-wrapper:hover) {
          background-color: transparent!important;
        }
        :global(.anticon svg) {
          color: #fafafa;
        }
        :global(button.rectangle--style) {
          align-self: center;
        }
        :global(.ant-input-affix-wrapper,  .ant-input-affix-wrapper > input.ant-input) {
          background: transparent;
          color: #fafafa;
        }
        :global(.ant-form) {
          width: 100%;
          margin-top: 15px;
        }
        :global(.ant-form-item) {
          margin-bottom: 16px;
        }
        @media screen and (max-width: 768px) {
          .newsletter--container {
            min-height: 350px;
          }
          .newsletter--content {
            padding: 0 25px;
            justify-content: space-evenly;
            flex-direction: column;
          }
          .newsletter--content :global(.input--container) {
            margin-right: 0;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default Newsletter;
