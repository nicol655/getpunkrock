import React, { Fragment } from "react";
import { useHistory } from "react-router-dom";

const Footer = () => {
  let history = useHistory();
  const hide_for_future = false;
  return (
    <Fragment>
      <div className="footer--container">
        <div className="footer--content">
          <div className="footer--list mapsite">
            <ul>
              <h3>Explora</h3>
              <li>
                <button
                  onClick={() => {
                    history.push("/");
                  }}
                >
                  Inicio
                </button>
              </li>
              <li>
                <button
                  onClick={() => {
                    history.push("/tienda");
                  }}
                >
                  Tienda
                </button>
              </li>
            </ul>
          </div>

          {hide_for_future && (
            <div className="footer--list">
              <ul>
                <h3>Ayuda y FAQ</h3>
                <li>¿Cómo comprar?</li>
                <li>Códigos de promo</li>
                <li>Envíos</li>
                <li>Compras internacionales</li>
              </ul>
            </div>
          )}
          
          <div className="footer--social">
            <div className="social--section">
              <h3>Síguenos en nuestras redes</h3>
              <a href="https://www.facebook.com/GetPunkRock" rel="noreferrer noopener" target="_blank"><span className="mdi mdi-facebook"></span></a>
              <a href="https://www.instagram.com/getpunkrock/" rel="noreferrer noopener" target="_blank"><span className="mdi mdi-instagram"></span></a>
              
            </div>
            <div className="social--section">
              <h3>¿Quiéres vender con nosotros?</h3>
              <a href="https://wa.link/h00q0f" rel="noreferrer noopener" target="_blank">
                <span className="mdi mdi-whatsapp"></span>
                <h4>Envíanos un whatsapp</h4>
              </a>
            </div>
          </div>
        </div>
        <div className="footer--legals">
          <div className="separator">
            <button>Políticas de privacidad</button>
          </div>
          <div className="separator">
            <button>Términos y condiciones</button>
          </div>
          <div className="separator">
            Derechos reservados
            <span className="mdi mdi-copyright"></span> 2020
          </div>
        </div>
      </div>
      <style jsx>
        {`
          .footer--container {
            max-height: 100%;
            background-color: #212121;
            display: flex;
            flex-direction: column;
            position: fixed;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 250px;
            background: #212121;
          }
          .footer--content {
            overflow: hidden;
            display: flex;
            align-items: baseline;
            align-items: center;
            justify-content: space-around;
            flex-grow: 1;
            height: auto;
          }
          .footer--social {
            display: flex;
            flex-direction: column;
            padding-left: 15px;
          }
          .footer--list {
            display: flex;
            justify-content: center;
          }
          .footer--list ul{
            list-style: none;
            color: #fafafa;
            padding-left: 15px;
          }
          .footer--list ul li{
            font-size: 12px;
          }
          .mapsite ul {
            list-style: none;
            color: #fafafa;
          }
          .mapsite ul li{
            border-width: 2px;
            border-style: solid;
            text-transform: uppercase;
            border-image: linear-gradient(to left, #00fa00, #212121) 0 0 1 0;
          }
          span {
            color: #00fa00;
            font-size: 1.4em;
            padding: 0 10px;
          }
          .social--section {
            align-items: center;
            justify-content: space-between;
          }
          h3 {
            text-transform: uppercase;
            color: #fafafa;
            font-family: Oswald-Medium;
            font-weight: lighter;
            letter-spacing: 2.5px;
          }
          h4 {
            display: inline-flex;
            color: #fafafa;
            margin: 0;
          }
          .footer--container .footer--legals .separator {
            font-size: 12px;
          }
          button {
            color: #fafafa;
            font-size: 12px;
            padding: 0 5px;
            text-align: center;
            font-family: nunito;
            letter-spacing: 1.2px;
            cursor: pointer;
          }
          .footer--legals {
            position: relative;
            color: #fafafa;
            margin: 0;
            padding: 10px 0px;
            display: flex;
            align-self: center;
            align-items: flex-end;
          }
          .separator {
            padding: 0 5px;
            text-align: center;
            border-left: 1px solid #4b585f;
            border-right: 1px solid #4b585f;
          }
          .separator a {
            color: #4b585f;
            text-decoration: none;
          }
          .separator:first-child {
            border-right: 0;
            border-left: 0;
          }
          .separator:last-child {
            border: 0;
            align-items: center;
          }
          @media screen and (max-width: 768px) {
            .footer--container .footer--legals .separator button, 
            .footer--container .footer--legals .separator {
              font-size: 10px;
            }
            .footer--container {
              height: 200px;
            }
          }
          @media screen and (max-width: 768px) {
            .separator {
              display: flex;
              flex-direction: row;
              justify-content: center;
              align-items: flex-end;
              text-align: center;
            }
            .footer--content {
              text-align: center;
              flex-wrap: wrap;
              position: relative;
            }
          }
          @media screen and (max-width: 425px) {
            .footer--list.mapsite {
              padding-left: 0;
            }
            .footer--container {
              height: 350px;
            }
            .footer--container .footer--content {
              flex-direction: column;
              flex-wrap: inherit;
            }
          }
          @media screen and (max-width: 320px) {
            
            .footer--container .footer--content .footer--list {
              justify-content: left;
            }
          }
        `}
      </style>
    </Fragment>
  );
};
export default Footer;
