/*eslint no-useless-escape: "off"*/
import React, { useEffect, useState } from "react";
import Media from "react-media";
import { Form, Radio } from 'antd';

const enums_endpoint = process.env.REACT_APP_ENUMS_ENDPOINT;
const brands_endpoint = process.env.REACT_APP_BRANDS_ENDPOINT;
const spanish_filters = {category: "Prenda", gender: "Género", brand: "Marca"};

function getCategories() {
  return new Promise((resolve) => {
    fetch(`${enums_endpoint}categories`)
    .then((res) => res.json())
    .then((data) => {
      resolve(data);
    });
  });
}
function getGenders() {
  return new Promise((resolve) => {
    fetch(`${enums_endpoint}genders`)
    .then((res) => res.json())
    .then((data) => {
      resolve(data);
    });
  });
}
function getBrands() {
  return new Promise((resolve) => {
    fetch(`${brands_endpoint}`)
    .then((res) => res.json())
    .then((data) => {
      resolve(data);
    });
  });
}

const Filter = ({ handleFiltersVisibility, filterChange }) => {
  const [filters, setFilters] = useState({category: [], gender: [], brand:[] });

  useEffect(() => {
    getCategories().then(categories=>{
      getGenders().then(genders=>{
        getBrands().then(brands=>{
          setFilters({category: categories, gender: genders, brand: brands });
        });
      });
    });
  },[]);

  const onChange = e => {
    e.stopPropagation();
    filterChange(e);
  };

  return (
    <>
      <div className="filter--list__container">
        <ul className="filter--list">
          <Media
            query="(max-width: 768px)"
            render={() => (
              <span
                onClick={handleFiltersVisibility}
                className="close--filters mdi mdi-close"
              ></span>
            )}
          />
          {Object.entries(filters).map((option, i) => {
            return (
              <div className="option--content"  key={`option-list-${i}`}>
                <h3 className="option--title">{spanish_filters[option[0]]}</h3>
                <Form.Item name={option[0]} className="custom--row">
                    <Radio.Group className="custom--radio" name={option[0]} onChange={onChange}>
                      {option[1]?.map((op_variant, i2) => {
                        return (
                          <Radio value={op_variant.name} key={`filter-option-${i2}`}>{op_variant.name}</Radio>
                          );
                      })}
                    </Radio.Group>
                  </Form.Item>
              </div>
            );
          })}
        </ul>
      </div>
     

      <style jsx>
        {`
          .filter--chips {
            position: absolute;
          }
          :global(.custom--radio) {
            display: flex;
            flex-direction: column;
          }
          :global(.custom--row) {
            margin-bottom: 0;
          }
          :global(.ant-tag){
            border-color: #00fa00;
            border-width: 2px;
            color: #212121;
            font-weight: bold;
          }
          .filter--list {
            margin-right: 20px;
          }
          .filter--variants {
            display: flex;
            margin: 5px 0;
            position: relative;
          }
          .filter--variants label {
            cursor: pointer;
            flex-grow: 1;
          }
          .filter--variants input[type="radio"] {
            position: absolute;
            visibility: hidden;
            left: 0;
          }
          .filter--variants input[type="radio"]:checked + span:before {
            content: "\F012F";
            color: #00fa00;
          }
          .filter--variants input[type="radio"] + span:before {
            content: "\F0130";
            color: grey;
          }
          
          .filter--list,
          .filter--variants {
            padding: 0;
            list-style: none;
          }
          .filter--variants button {
            display: flex;
            align-items: center;
            padding: 0;
            margin: 5px 0;
            cursor: pointer;
            color: grey;
          }
          
          .close--filters {
            position: absolute;
            top: 10px;
            right: 10px;
            color: #00fa00;
            font-size: 20px;
          }

          .filter--list .option--title {
            color: #212121;
            border-bottom: 1px solid #00fa00;
            margin: 10px 0;
            padding-bottom: 2px;
            border-width: 2px;
          }

          @media screen and (max-width: 768px) {
            :global(.accordion--element h4), :global(.ant-form label) {
              color: #fafafa;
            }
            .filter--list__container {
              position: fixed;
              left: -100%;
              z-index: 9;
              background: #212121;
              color: #fafafa;
              margin: 0;
              width: 0px;
              overflow: hidden;
              top: 0;
              height: 100vh;
              max-height: 100vh;
            }
            .filter--list {
              justify-content: center;
              display: flex;
              flex-direction: column;    
              height: 100%;
              justify-content: flex-start;
              margin: 0;
              overflow-y: scroll;
            }
            .filter--list .option--title {
              color: #fafafa;
            }
            :global(.products--container.show--filters__mobile) .filter--list__container {
              left: 0;
              width: 50vw;
              transition: all 0.3s ease;
              padding: 50px 15px;
            }
            
          }
        `}
      </style>
    </>
  );
};
export default Filter;
