const filterReducer = (state, action) => {
  let new_activeFilters = [];

  let isActive = state.filters.filter((f) => {
    return f.option === action.name;
  });
  if (isActive.length) {
    new_activeFilters = state.filters;
    new_activeFilters.map((f, i) => {
      if (f.option === action.name) new_activeFilters.splice(i, 1);
      return new_activeFilters;
    });
  } else {
    new_activeFilters = [
      ...state.filters,
      { parent: action.parent, option: action.name },
    ];
  }

  return { filters: new_activeFilters };
};
export default filterReducer;
