import React, { useEffect } from "react";
import insta1 from "../../assets/images/instagram_test/product1.jpg";
import insta2 from "../../assets/images/instagram_test/product2.jpg";
import insta3 from "../../assets/images/instagram_test/product3.jpg";
const SocialGrid = ({ frame, ...rest }) => {
  // const setInstagramFeed = () => {
  //   const script = document.createElement('iframe');
  //   script.async = 1;
  //   script.type = 'text/javascript';
  //   script.src ='https://cdn.curator.io/published/78a05fff-55d5-4c62-acab-86b35fa33442_364o8d4w.js';
  //   let e = document.getElementById("social_grid");
  //   e.parentNode.insertBefore(script, e);
  // }
  // useEffect(() => {
  //   setInstagramFeed();
  //   console.log("set feed")
  // },[])
  return (
    <>
      <div {...rest} className={`socialGrid--container ${frame}`}>
        <div className="item1 desktop">
          <img src={insta2} alt="instagram product 1" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item2">
          <img src={insta1} alt="instagram product 2" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item3">
          <img src={insta2} alt="instagram product 3" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item4">
          <img src={insta3} alt="instagram product 4" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item5 desktop">
          <img src={insta1} alt="instagram product 5" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item6">
          <img src={insta2} alt="instagram product 6" />
          <span className="mdi mdi-instagram"></span>
        </div>
        <div className="item7">
          <img src={insta3} alt="instagram product 7" />
          <span className="mdi mdi-instagram"></span>
        </div>
      </div>
      <style jsx>{`
        .socialGrid--container {
          display: grid;
          grid-template-columns: repeat(2, 1fr) 2fr repeat(2, 1fr);
          grid-template-rows: repeat(2, 30vh);
          grid-column-gap: 0px;
          grid-row-gap: 0px;
          transition: transform 1s linear;
        } 
        .socialGrid--container div {
          transition: transform 1s linear;
          border: 1px solid;
          display: flex;
          justify-content: flex-end;
          transform: initial;
        }
        .socialGrid--container img {
          width: 100%;
          height: 100%;
          object-fit: cover;
          display: flex;
          filter: brightness(0.9);
        }
        .socialGrid--container span {
          position: absolute;
          display: flex;
          align-self: flex-end;
          justify-content: flex-end;
          color: #000;
          font-size: 25px;
        }
        .socialGrid--container .item1 {
          grid-area: 1 / 5 / 3 / 6;
          transform: translate(70px, 15px);
        }
        .socialGrid--container .item2 {
          grid-area: 1 / 4 / 2 / 5;
          transform: translate(40px, -100px);
        }
        .socialGrid--container .item3 {
          grid-area: 2 / 4 / 3 / 5;
          transform: translate(110px, -20px);
        }
        .socialGrid--container .item4 {
          grid-area: 1 / 3 / 3 / 4;
        }
        .socialGrid--container .item5 {
          grid-area: 1 / 1 / 2 / 3;
          transform: translate(-90px, -120px);
        }
        .socialGrid--container .item6 {
          grid-area: 2 / 1 / 3 / 2;
          transform: translate(-20px, -130px);
        }
        .socialGrid--container .item7 {
          grid-area: 2 / 2 / 3 / 3;
          transform: translate(-105px, 10px);
        }
        .socialGrid--container.frame1 .item1 {
          transform: translate(70px, 15px);
        }
        .socialGrid--container.frame1 .item2 {
          transform: translate(40px, -100px);
        }
        .socialGrid--container.frame1 .item3 {
          transform: translate(110px, -20px);
        }
        .socialGrid--container.frame1 .item5 {
          transform: translate(-90px, -90px);
        }
        .socialGrid--container.frame1 .item6 {
          transform: translate(-20px, -130px);
        }
        .socialGrid--container.frame1 .item7 {
          transform: translate(-105px, 10px);
        }

        .socialGrid--container.frame2 .item1 {
          transform: translate(40px, 20px);
        }
        .socialGrid--container.frame2 .item2 {
          transform: translate(25px, -50px);
        }
        .socialGrid--container.frame2 .item3 {
          transform: translate(60px, -10px);
        }
        .socialGrid--container.frame2 .item5 {
          transform: translate(-40px, -60px);
        }
        .socialGrid--container.frame2 .item6 {
          transform: translate(-10px, -70px);
        }
        .socialGrid--container.frame2 .item7 {
          transform: translate(-55px, 20px);
        }

        .socialGrid--container.frame3 .item1 {
          transform: translate(20px, 25px);
        }
        .socialGrid--container.frame3 .item2 {
          transform: translate(12px, -50px);
        }
        .socialGrid--container.frame3 .item3 {
          transform: translate(30px, -5px);
        }
        .socialGrid--container.frame3 .item5 {
          transform: translate(-20px, -30px);
        }
        .socialGrid--container.frame3 .item6 {
          transform: translate(-10px, -70px);
        }
        .socialGrid--container.frame3 .item7 {
          transform: translate(-20px, 25px);
        }
        
        .socialGrid--container.frame4 div {
          transform: translate(0px, 0px);
        }
        @media screen and (max-width: 768px) {
          .socialGrid--container {
            grid-template-columns: 1fr;
            grid-column-gap: 2px;
            grid-row-gap: 2px;
          }
          .socialGrid--container .item6 {
            grid-area: 1 / 2 / 2 / 3;
          }
        }
      `}</style>
    </>
  );
};
export default SocialGrid;
