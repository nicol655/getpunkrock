import React from "react";

const HiddenImage = ({ rows, columns, text, image, parallax }) => {
  const childrens = [];
  const handleHover = (e) => {
    
    let item = document.getElementById(e.target.id);
    !item.classList.contains("visible")
      ? item.classList.add("visible")
      : e.preventDefault();
  };

  for (let i = 0; i < columns * rows; i++) {
    childrens.push(
      <div
        key={`hidden_image_${i}`}
        onMouseEnter={(e) => {
          handleHover(e);
        }}
        className={i + 1 > 12 ? "desktop" : ""}
        id={`item${i + 1}`}
      >
        {text}
      </div>
    ); 
  }
  return (
    <>
      <div className="hidden--product">{childrens}</div>
      <style jsx>
        {`
          .hidden--product {
            display: grid;
            grid-column-gap: 0px;
            grid-row-gap: 0px;
            height: 60vh;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            align-items: center;
            justify-items: center;
            margin-bottom: 50px;
            background-image: url(${image});
            grid-template-columns: repeat(6, 1fr);
            grid-template-rows: repeat(4, 1fr);
            background-attachment: ${parallax ? "fixed" : "none"};
          }
          .hidden--product > :global(div) {
            background: #212121;
            color: #fafafa;
            text-transform: uppercase;
            justify-content: center;
            align-content: center;
            cursor: pointer;
            display: grid;
            height: 100%;
            width: 100%;
            background: rgba( 0, 0, 0, 0.70 );
            backdrop-filter: blur( 15.0px );
          }
          .hidden--product :global(.visible) {
            background: transparent;
            color: transparent;
            backdrop-filter: blur( 0 );
          }
          @media screen and (max-width: 768px) {
            .hidden--product {
              grid-template-columns: repeat(3, 1fr);
              grid-template-rows: repeat(4, 1fr);
            }
          }
        `}
      </style>
    </>
  );
};
export default HiddenImage;
