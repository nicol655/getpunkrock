import React from "react";

const Loader = () => {
  return (
    <>
      <div className="loader--container">
        <div className="loader">
          Getpunkrock
          <span></span>
        </div>
      </div>
      <style jsx>
        {`
          .loader--container {
            display: flex;
            position: fixed;
            background-color: #212121;
            width: 100vw;
            height: 100vh;
            z-index: 9999;
            align-items: center;
            justify-content: center;
          }
          .loader {
            position: absolute;
            width: 160px;
            height: 160px;
            background: transparent;
            border: 3px solid #9b9191;
            text-align: center;
            border-radius: 50%;
            line-height: 150px;
            font-size: 15px;
            color: #00fa00;
            letter-spacing: 4px;
            text-transform: uppercase;
            text-shadow: 0 0 5px #ffffff;
            box-shadow: 0 0 20px #00fa00;
          }
          .loader:before {
            content: "";
            position: absolute;
            top: -3px;
            left: -3px;
            width: 100%;
            height: 100%;
            border: 3px solid transparent;
            border-bottom: 3px solid #00fa00;
            border-top: 3px solid #00fa00;
            border-radius: 50%;
            animation: animateC 2s linear infinite;
          }
          @keyframes animateC {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
          @keyframes animate {
            0% {
              transform: rotate(45deg);
            }
            100% {
              transform: rotate(405deg);
            }
          }
        `}
      </style>
    </>
  );
};
export default Loader;
