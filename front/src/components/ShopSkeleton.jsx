/*eslint no-shadow-restricted-names: "off"*/
import React from "react";
import { Skeleton } from 'antd';

const ShopSkeleton = ({size}) => {
  return (
    <>
      {[...Array(parseInt(size))].map((undefined,i) => {
        return(
          <div key={`skeleton-element-${i}`}>
            <Skeleton.Image/>
            <Skeleton active size="small"/>
          </div>
        )
      })}
    </>
  );
}

export default ShopSkeleton;