const CracoLessPlugin = require('craco-less');

module.exports = {
  
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { '@primary-color': '#00fa00' },
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: require('craco-styled-jsx'),
      options: {
        sass: true, // Required node-sass to enable this option
        cssFileSupport: true, // Allow to write css in a standalone file
        cssFileTest: /\.styled\.(s)css$/,
      }
    },
  ],
};