from django.conf import settings
from django.conf.urls import handler404, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from django.views.static import serve
from rest_framework import routers

from brands.views import BrandViewSet
from products.views import (ImageViewSet, ProductViewSet,
    ProductsPerProductViewSet)
from users.views import PerfilViewSet, UserViewSet
from images.views import ImagesViewSet
from subscribers.views import SubscribersViewSet
from payment.views import PaymentViewSet


router = routers.DefaultRouter()
router.register(r'brands', BrandViewSet)
router.register(r'images', ImageViewSet)
router.register(r'hidden_image', ImagesViewSet)
router.register(r'sub_products', ProductsPerProductViewSet)
router.register(r'products', ProductViewSet)
router.register(r'perfils', PerfilViewSet)
router.register(r'users', UserViewSet)
router.register(r'subscribers', SubscribersViewSet)
router.register(r'payment', PaymentViewSet)


urlpatterns = [
    url(r'api/', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'api/products/', include('products.urls')),
    url(r'backoffice/', include('backoffice.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


handler404 = 'backoffice.views.error_404'