"""Payment Enums module."""
from django_enumfield import enum


class PaymentStatusEnum(enum.Enum):

  GENE = 0 #Order generated and waiting for MP response.
  APRO = 1 #Payment approved.
  CONT = 2 #Payment pending.
  OTHE = 3 #Rejected by general error.
  CALL = 4 #Rejected with validation approval.
  FUND = 5 #Rejected by insufficient funds.
  SECU = 6 #Rejected by invalid CVV.
  EXPI = 7 #Rejected by invalid expiracy date.
  FORM = 8 #Rejected by form error.

  __default__ = GENE