"""Module Serializer for Payment."""
from os import environ
from rest_framework import serializers

from .models import PaymentOrder, PaymentOrderProduct
from products.serializers import ProductPerProductSerializer


class PaymentOrderProductSerializer(serializers.ModelSerializer):

  """Payment order serializer class representation."""

  products = ProductPerProductSerializer(many=True, read_only=True)

  class Meta:
    model = PaymentOrderProduct
    fields = ['id', 'products']
    

class PaymentOrderSerializer(serializers.ModelSerializer):

  """Payment order serializer class representation."""

  products = PaymentOrderProductSerializer(many=True, read_only=True)

  class Meta:
    model = PaymentOrder
    fields = ['id', 'email', 'products', 'name', 'surname', 'street',
      'address_number', 'neighborhood', 'postal_code', 'terms',
      'status', 'mp_payment_id']