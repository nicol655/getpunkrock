"""Model module for payment."""
from django.db import models
from django_enumfield import enum

from .enums import PaymentStatusEnum
from products.models import ProductPerProduct

import uuid


class PaymentOrder(models.Model):

  """Class model representation for payment order."""

  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  email = models.CharField(max_length=50, blank=False)
  name = models.CharField(max_length=20, blank=False)
  surname = models.CharField(max_length=20, blank=False)
  street = models.CharField(max_length=30, blank=False)
  address_number = models.CharField(max_length=20, blank=False)
  neighborhood = models.CharField(max_length=20, blank=False)
  postal_code = models.CharField(max_length=10, blank=False)
  terms = models.BooleanField(default=True)
  status = enum.EnumField(PaymentStatusEnum)

  mp_payment_id = models.CharField(max_length=50, blank=True)

  created_at = models.DateTimeField(auto_now_add=True)
  modified_at = models.DateTimeField(blank=True, null=True)
  deleted_at = models.DateTimeField(blank=True, null=True)


class PaymentOrderProduct(models.Model):

  """Class model representation for payment order per product."""

  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  product = models.ForeignKey(ProductPerProduct, 
    related_name="payment_order_subproduct", on_delete=models.DO_NOTHING)
  payment_order = models.ForeignKey(PaymentOrder,
    related_name="payment_order_order", on_delete=models.DO_NOTHING)
  quantity = models.IntegerField(blank=False)