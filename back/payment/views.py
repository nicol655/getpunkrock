"""ViewSet Module for payment."""
from rest_framework import request, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from products.models import ProductPerProduct

from .models import PaymentOrder, PaymentOrderProduct
from .serializers import PaymentOrderSerializer

import logging
import json


logger = logging.getLogger(__name__)


class PaymentViewSet(viewsets.ModelViewSet):

  """Payment ViewSet class representation."""

  queryset = PaymentOrder.objects.all().filter(deleted_at=None)
  serializer_class = PaymentOrderSerializer

  def create(self, request, *args, **kwargs):
    """Preset user and brand before create a product."""
    products = json.loads(request.body).get("products")
    response = super().create(request, *args, **kwargs)
    payment = PaymentOrder.objects.filter(id=response.data.get("id")).first()
    payment_orders = []
    for product in products:
      product_object = ProductPerProduct.objects.filter(id=product["id"]).first()
      payment_orders.append(PaymentOrderProduct(product=product_object, 
                                                quantity=product["quantity"],
                                                payment_order=payment))
    PaymentOrderProduct.objects.bulk_create(payment_orders)
    response = {'status': 'payment order generated', 
                'payment_order_id': response.data.get("id")}
    return Response(response, status=status.HTTP_200_OK)