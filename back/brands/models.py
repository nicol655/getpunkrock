"""Model module for Brands."""
from django.db import models
from django.contrib.auth.models import User

import uuid


class Brand(models.Model):

    """Class model representation for Brands."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30, blank=False)
    website_url = models.CharField(max_length=50, blank=True, default="")

    created_by = models.ForeignKey(User, related_name='brand_created_by',
        on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, related_name='brand_modified_by',
        on_delete=models.DO_NOTHING, blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=True, blank=False)