"""Serializer module for brands."""
from rest_framework import serializers

from .models import Brand


class BrandSerializer(serializers.ModelSerializer):

    """Brand Serializer class representation."""

    class Meta:
        model = Brand
        fields = ['id', 'name', 'website_url']