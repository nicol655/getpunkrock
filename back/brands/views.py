"""View module for brands."""
from datetime import datetime

from django.contrib.auth.models import User
from django.http import QueryDict
from rest_framework import viewsets, request, status
from rest_framework.response import Response
from django.utils import timezone

from .models import Brand
from .serializers import BrandSerializer


class BrandViewSet(viewsets.ModelViewSet):

    """Brand ViewSet class representation."""

    queryset = Brand.objects.filter(deleted_at__isnull=True).all()
    serializer_class = BrandSerializer

    def perform_create(self, serializer):
        user = User.objects.get(pk=self.request.data['user_id'])
        serializer.save(created_by=user)

    def destroy(self, request, pk):
        try:
            brand = Brand.objects.get(pk=pk)
            brand.deleted_at = datetime.now(tz=timezone.utc)
            brand.save()
            return Response(status=status.HTTP_200_OK)
        except:
            print('Something went wrong while deleting the brand')
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def update(self, request, pk):
        try:
            brand = Brand.objects.get(pk=pk)
            brand.name = request.data.get('name')
            brand.website_url = request.data.get('website_url')
            brand.modified_by = User.objects.get(pk=request.data['user_id'])
            brand.modified_at = datetime.now(tz=timezone.utc)
            brand.save()
            return Response(status=status.HTTP_200_OK)
        except:
            print('Something went wrong while deleting the brand')
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

