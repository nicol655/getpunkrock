"""Module Serializer for Hashes."""
from os import environ
from rest_framework import serializers

from .models import Hash


class HashSerializer(serializers.ModelSerializer):

    """Hashes serializer class representation."""

    class Meta:
        model = Hash
        fields = ['id', 'hash', 'body', 'created_at']