"""Model module for Hashes."""
from django.db import models

import uuid


class Hash(models.Model):

    """Class model representation for Hashes."""

    hash = models.UUIDField(primary_key=True, default=uuid.uuid4,
        editable=False)
    body = models.CharField(max_length=500, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)