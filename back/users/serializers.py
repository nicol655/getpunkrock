"""Serializer module for Users."""
from django.contrib.auth.models import User as django_user
from rest_framework import serializers

from users.models import User, Perfil


class PerfilSerializer(serializers.HyperlinkedModelSerializer):

    """Perfil Serializer class representation."""

    class Meta:
        model = Perfil
        fields = ['id', 'name']


class UserSerializer(serializers.HyperlinkedModelSerializer):

    """User Serializer class representation."""

    perfil = PerfilSerializer()

    class Meta:
        model = User
        fields = ['id', 'created_at', 'name', 'email', 'perfil']


class CurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = django_user
        fields = ('username', 'email', 'id')