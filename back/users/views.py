"""View module for users."""
from rest_framework import viewsets, request
from rest_framework.response import Response

from users.models import User, Perfil
from users.serializers import PerfilSerializer, UserSerializer


class UserViewSet(viewsets.ModelViewSet):

    """User ViewSet class representation."""

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request):
        """Override create user."""
        data = request.data
        perfil = Perfil.objects.get(id=request.data.get('perfil_id'))
        
        user = User(name=data.get('name'), email=data.get('email'), 
                    perfil=perfil)
        user.save()
        return Response({'response': 'user created'})


class PerfilViewSet(viewsets.ModelViewSet):

    """Perfil ViewSet class representation."""

    queryset = Perfil.objects.all()
    serializer_class = PerfilSerializer