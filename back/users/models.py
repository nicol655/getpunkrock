"""Model module for users."""
from django.db import models

import uuid


class Perfil(models.Model):

    """Class model representation for perfil."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=20, blank=False)

    class Meta:
        ordering = ['name']


class User(models.Model):

    """Class model representation for users."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=30, blank=False)
    email = models.CharField(max_length=50, blank=False)
    password = models.CharField(max_length=20, blank=True)
    perfil = models.ForeignKey(Perfil, related_name='perfil',
                               on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['created_at']