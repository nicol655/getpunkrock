from django.contrib import admin
from users.models import Perfil, User

admin.site.register(Perfil)
admin.site.register(User)