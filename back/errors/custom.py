"""Custom Errors module."""


class Error(Exception):
  """Error base class representation."""
  pass


class AlreadyUnsubscribedError(Error):
  """Error Already Unsubscribred class representation."""
  pass