"""Module Serializer for Subscriber."""
from os import environ
from rest_framework import serializers

from .models import Subscriber


class SubscriberSerializer(serializers.ModelSerializer):

    """Subscriber serializer class representation."""

    class Meta:
        model = Subscriber
        fields = ['id', 'email', 'last_name', 'first_name', 'enabled']