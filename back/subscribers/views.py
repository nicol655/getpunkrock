"""ViewSet Module for Subscribers."""
from django.core.mail import send_mail
from rest_framework import status, viewsets
from rest_framework.response import Response

from .models import Subscriber
from .serializers import SubscriberSerializer
from hashes.models import Hash 
from services import EmailSenderService
from errors.custom import AlreadyUnsubscribedError

import logging


logger = logging.getLogger(__name__)


class SubscribersViewSet(viewsets.ModelViewSet):

    """Subscribers ViewSet class representation."""

    queryset = Subscriber.objects.all()
    serializer_class = SubscriberSerializer

    def create(self, request, *args, **kwargs):
        """Override create function to create a new subscriber if not exist and
        then send an welcome email."""
        if not len(self.queryset.filter(email=request.data.get("email"))):
            resp = super().create(request, *args, **kwargs)
            hashed = Hash(body=self.set_body_email(request))
            hashed.save()
            EmailSenderService().send_welcome_subscriber_email(
                name="{} {}".format(request.data.get("last_name"),
                    request.data.get("first_name")),
                recipient=request.data.get("email"),
                user_hash=str(hashed.hash))
        else:
            resp = Response(
                {'message': 'Email already registered'},
                status=status.HTTP_400_BAD_REQUEST)
        return resp

    def retrieve(self, request, *args, **kwargs):
        """Override retrieve function to disable user from subscriptions."""
        _hash=kwargs.get('pk')
        try:
            hashed = Hash.objects.filter(hash=_hash).first()
            subscriber_email = eval(hashed.body).get("email")
            subscriber = self.queryset.filter(email=subscriber_email).first()
            if not subscriber.enabled:
                raise AlreadyUnsubscribedError
            subscriber.enabled = False
            subscriber.save()
            resp =  Response(
                {'message': 'successfully unsubscribed'},
                status=status.HTTP_200_OK)
        except AlreadyUnsubscribedError:
            resp = Response(
                {'message': 'Link expired or user already unsubscribed'},
                status=status.HTTP_403_FORBIDDEN)
        except Exception:
            resp = Response(
                {'message': 'The hash is invalid'},
                status=status.HTTP_400_BAD_REQUEST)
        return resp

    def set_body_email(self, query_dict):
        """Generate body hash with the subscriber's queryDict model object.
        
        Arguments
        ---------
            query_dict : QueryDict
                subscriber's model object.

        Returns
        -------
            string'ed dict with the subscriber information.
        """
        return str({
            "email": query_dict.data.get("email"),
            "first_name": query_dict.data.get("first_name"),
            "last_name": query_dict.data.get("last_name"),
        })