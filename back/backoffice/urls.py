from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from backoffice.views import (BrandList, BrandDetails, error_404, index, 
    logout_view, login_set_user, ProductList, GetEnums, SubProductList,
    SubProductDetails, ProductDetails, EnableDisableProduct)


urlpatterns = [
    path('login/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('login/user/', login_set_user, name="get_login_user"),
    path('logout', logout_view, name="logout"),
    path('index', index, name="index"),
    path('404', error_404, name="error_404"),
    path('gateway/brands/', BrandList.as_view()),
    path('gateway/brands/<pk>/', BrandDetails.as_view()),
    path('gateway/products/', ProductList.as_view()),
    path('gateway/products/<pk>/', ProductDetails.as_view()),
    path('gateway/products/<pk>/enable-disable/',
        EnableDisableProduct.as_view()),
    path('gateway/sub_products/', SubProductList.as_view()),
    path('gateway/sub_products/<pk>/', SubProductDetails.as_view()),
    path('gateway/products/get_enums', GetEnums.as_view()),
]