from django.shortcuts import render_to_response


def authentication_required(funct):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return render_to_response("backoffice/404.html")
        return funct(request, *args, **kwargs)
    return wrapper