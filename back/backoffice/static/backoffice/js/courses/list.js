tinymce.init({selector:'textarea#courseContentTextArea'});
tinymce.init({selector:'textarea#createCourseContentTextArea'});

axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "XCSRF-TOKEN";

var courses_list = new Vue({
  el: '#courses_list',
  data: {
    endpoint: '/courses/',
    section_name: 'Talleres',
    csrftoken: $('#csrf_token').val(),
    courses: [],
    delete_course: {
      id: '',
      title: ''
    },
    restore_course: {
      id: '',
      title: ''
    },
    alert: {
      title: '',
      message: '',
      type: ''
    },
    edit_course: {
      title: '',
      price: '',
      duration: 3,
      date: '',
      time: '',
      discount: 0,
      teacher: '',
      created_by: '',
      description: '',
      content: '',
      location: '',
      thumbnail_image_url: ''
    },
    create_course: {
      title: '',
      price: '',
      duration: 3,
      date: '',
      time: '',
      discount: '',
      teacher: '',
      description: '',
      content: '',
      location: '',
      thumbnail_image_url: ''
    },
    imageChanged: false
  },
  methods:{
    list_courses: async function(){
      await axios
        .get(this.endpoint)
        .then(response => {
          this.courses = response.data
          $('#dataTable').DataTable({
            paging: false,
            searching: false,
            ordering:  false
          })
        })
    },

    openDeleteModal: function(title, id, e){
      if (!e.target.getAttribute('data-disabled')){
        this.delete_course.title = title
        this.delete_course.id = id
        $('#deleteModal').modal()
      }
    },

    openRestoreModal: function(title, id, e){
      if (!e.target.getAttribute('data-disabled')){
        this.restore_course.title = title
        this.restore_course.id = id
        $('#restoreModal').modal()
      }
    },

    openEditModal: function(id, e){
      if (!e.target.getAttribute('data-disabled')){
        $('#selectedImageName').text('')
        $('#courseImageFile').val('')
        this.courses.map(course => {
          if (course.id == id){
            tinyMCE.get('courseContentTextArea').setContent(course.content)
            this.edit_course = course
            this.formatCommingDateTime()
            $('#coursePreviewImage').css('background-image', `url("${course.thumbnail_image_url}")`)
          }
        })
        $('#editModal').modal()
      }
    },

    openCreateModal(){
      $('#createModal').modal()
    },

    toggleAlert(title, msg, _type){
      this.alert.title = title
      this.alert.message = msg
      this.alert.type = _type
      setTimeout(function(){
        $('#alertSection').removeClass('d-none').addClass('show')
      }, 200)
      setTimeout(function(){
        $('#alertSection').removeClass('show').addClass('d-none')
      }, 5000)
    },

    deleteCourse: function(id){
      axios
        .delete(this.endpoint + id + '/',
          {
            headers: {'csrfmiddlewaretoken': this.csrftoken}
          }
        )
        .then(response => {
          this.courses.map(course => {
            if (course.id == id)
              course.deleted_at = response.data.deleted_at

            $('#deleteModal').modal('hide')
            this.toggleAlert('Deshabilitado', 
              `el taller ${this.edit_course.title} satisfactoriamente.`,
              'alert-warning')
          })
        })
    },

    restoreCourse(){
      axios
        .put(`${this.endpoint}restore/${this.restore_course.id}`,
          {
            headers: {'csrfmiddlewaretoken': this.csrftoken}
          }
        )
        .then(response => {
          this.courses.map(course => {
            if (course.id == this.restore_course.id)
              course.deleted_at = null

            $('#restoreModal').modal('hide')
            this.toggleAlert('Habilitado', 
              `el taller ${this.edit_course.title} satisfactoriamente.`,
              'alert-success')
          })
        })
    },

    editCourse: function(){
      let fdata = this.serializeData()
      axios
        .put(this.endpoint + this.edit_course.id + '/', 
          fdata,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
              'csrfmiddlewaretoken': this.csrftoken
            }
          }
        )
        .then(response => {
          this.courses.map((course, index) => {
            if (course.id == response.data.course.id)
              this.courses[index] = response.data.course
          })
          this.imageChanged = false
          $('#editModal').modal('hide')
          this.toggleAlert('¡Perfecto!', 
            `El taller ${this.edit_course.title} ha sido modificado.`,
            'alert-success')
        })
    },

    createCourse(){
      if(this.validateForm(this.create_course)){
        let fdata = this.serializeCreateData()
        axios
          .post(this.endpoint,
            fdata,
            {
              headers: {
                'Content-Type': 'multipart/form-data',
                'csrfmiddlewaretoken': this.csrftoken
              }
            }
          )
          .then(response => {
            alert('Curso creado satisfactoriamente')
            location.reload()
          })
      }
    },

    serializeCreateData(){
      let fdata = new FormData();

      this.create_course.start_at = `${this.create_course.date} ${this.create_course.time}:00`
      fdata.append('thumbnail_image_url',  this.create_course.thumbnail_image_url)
      fdata.append('title', this.create_course.title)
      fdata.append('price', this.create_course.price)
      fdata.append('discount', this.create_course.discount)
      fdata.append('duration', 3)
      fdata.append('location', this.create_course.location)
      fdata.append('start_at', this.create_course.start_at)
      fdata.append('teacher', this.create_course.teacher)
      fdata.append('created_by', $('#user_id').val())
      fdata.append('description', $('#createCourseDescriptionTextArea').val())
      fdata.append('content', tinymce.get('createCourseContentTextArea').getContent())
      return fdata
    },

    serializeData(){
      let fdata = new FormData();

      this.edit_course.start_at = `${this.edit_course.date} ${this.edit_course.time}:00`
      this.edit_course.price = this.edit_course.price.slice(2)
      this.edit_course.duration = this.edit_course.duration.slice(0,-4)

      if(this.imageChanged)
        fdata.append('thumbnail_image_url',  this.edit_course.thumbnail_image_url)

      fdata.append('title', this.edit_course.title)
      fdata.append('price', this.edit_course.price)
      fdata.append('discount', this.edit_course.discount)
      fdata.append('duration', 3)
      fdata.append('location', this.edit_course.location)
      fdata.append('start_at', this.edit_course.start_at)
      fdata.append('teacher', this.edit_course.teacher)
      fdata.append('created_by', this.edit_course.created_by.id)
      fdata.append('description', $('#courseDescriptionTextArea').val())
      fdata.append('content', tinymce.get('courseContentTextArea').getContent())
      return fdata
    },

    enableRestore: function(deleted_at){
      response_color = 'grey'
      if (deleted_at)
        response_color = 'green'
      return response_color
    },

    enableEditDelete: function(deleted_at, color){
      response_color = 'grey'
      if (!deleted_at)
        response_color = color
      return response_color
    },

    setDisable: function(deleted_at, color){
      ret = true
      if (color == 'green' && deleted_at)
        ret = false
      if (color != 'green' && !deleted_at)
        ret = false
      return ret
    },

    reloadThumbnail(ev){
      this.imageChanged = true
      $('#coursePreviewImage').removeAttr('style')
      this.edit_course.thumbnail_image_url = ev.target.files[0]
      let file = ev.target.files[0]
      let reader  = new FileReader()
      $('#selectedImageName').text(file.name)
      reader.addEventListener("load", function () {
        $('#coursePreviewImage').css('background-image', `url("${reader.result}")`)
      }.bind(this), false)
      if (file) reader.readAsDataURL(file)
    },

    createReloadThumbnail(ev){
      $('#createCoursePreviewImage').removeAttr('style')
      this.create_course.thumbnail_image_url = ev.target.files[0]
      let file = ev.target.files[0]
      let reader  = new FileReader()
      $('#createSelectedImageName').text(file.name)
      reader.addEventListener("load", function () {
        $('#createCoursePreviewImage').css('background-image', `url("${reader.result}")`)
      }.bind(this), false)
      if (file) reader.readAsDataURL(file)
    },

    formatCommingDateTime: function(){
      var dat = new Date(this.edit_course.date)
      this.edit_course.date = `${dat.getFullYear()}-${dat.getMonth()+1}-${dat.getDate()}`
      this.edit_course.time = this.edit_course.time.slice(0,-2)
    },

    validateForm(arrData){
      let validated = true
      try{
        if($('#createCourseDescriptionTextArea').val() == '' ||
        tinymce.get('createCourseContentTextArea').getContent() == ''){
          alert('Todos los campos son obligatorios')
          throw new TypeError(' this is null or not defined');
        }
        Object.keys(arrData).forEach(key => {
          if(arrData[key] == '' && key != 'content' && key != 'description'){
            console.log(key)
            alert('Todos los campos son obligatorios')
            throw new TypeError(' this is null or not defined');
          }
        })
      } catch{
        validated = false
      }
      return validated
    }
  },
  created(){
    this.list_courses()
  },
  delimiters: ["[[","]]"]
})