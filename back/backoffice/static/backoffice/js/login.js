var login = new Vue({
	el: '#login',
	data: {
    endpoint: '/backoffice/login/',
    csrftoken: $('#csrf_token').val(),
		credentials: {
			email: '',
			password: ''
    },
    alert: {
      type: 'alert-danger'
    },
	},
	methods: {
    onEnter(){
      this.attemptLogin()
    },

		attemptLogin(){
      let self = this
      axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
      axios.defaults.xsrfCookieName = "XCSRF-TOKEN";
      $('#alertSection').removeClass('show').addClass('d-none')
			axios
				.post(this.endpoint + 'attempt_login', 
          self.credentials,
          {
            headers: { "csrfmiddlewaretoken": this.csrftoken }
          }
        )
        .then(response => {
          if(response.status === 200)
            location.href = '/backoffice/index'
        })
        .catch(error => {
          $('#alertSection').removeClass('d-none').addClass('show')
        })
		}
	},
	delimiters: ["[[","]]"]
})