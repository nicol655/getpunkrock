var top_menu = new Vue({
  el: '#top-menu',
  data: {
    endpoint: '/backoffice/'
  },
  methods: {
    logout(){
      axios
        .get(this.endpoint + 'logout')
        .then(response => {
          if(response.status === 200)
            location.href = '/backoffice/'
        })
    }
  },
  delimiters: ["[[","]]"]
})
