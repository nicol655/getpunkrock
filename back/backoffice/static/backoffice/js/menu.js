var menu = new Vue({
    el: '#menu',
    data: {
        options: [
            {
                'title': 'Dashboard',
                'url': '/backoffice/index',
                'icon': 'fa-tachometer-alt',
                'active': window.location.href.includes('index')
            },
            {
                'title': 'Products',
                'url': '/backoffice/products',
                'icon': 'fa-tshirt',
                'active': window.location.href.includes('products')
            },
            {
                'title': 'Brands',
                'url': '/backoffice/brands',
                'icon': 'fa-tshirt',
                'active': window.location.href.includes('brands')
            }
        ]
    },
    delimiters: ["[[","]]"]
  })
