axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "XCSRF-TOKEN";

var brands_list = new Vue({
  el: '#brands_list',
  data: {
    endpoint: '/api/brands/',
    section_name: 'Brands',
    csrftoken: $('#csrf_token').val(),
    brands: [],
    delete_brand: {
      id: '',
      name: ''
    },
    restore_brand: {
      id: '',
      name: ''
    },
    alert: {
      title: '',
      message: '',
      type: ''
    },
    edit_brand: {
      id: '',
      name: '',
      website_url: ''
    },
    create_brand: {
      id: '',
      name: '',
      website_url: ''
    }
  },
  methods:{
    list_brands: async function(){
      await axios
        .get(this.endpoint)
        .then(response => {
          this.brands = response.data
          $('#dataTable').DataTable({
            paging: false,
            searching: false,
            ordering:  false
          })
        })
    },

    openDeleteModal: function(name, id, e){
      if (!e.target.getAttribute('data-disabled')){
        this.delete_brand.name = name
        this.delete_brand.id = id
        $('#deleteModal').modal()
      }
    },

    openRestoreModal: function(name, id, e){
      if (!e.target.getAttribute('data-disabled')){
        this.restore_brand.name = name
        this.restore_brand.id = id
        $('#restoreModal').modal()
      }
    },

    openEditModal: function(id, e){
      if (!e.target.getAttribute('data-disabled')){
        this.brands.map(brand => {
          if (brand.id == id){
            this.edit_brand = brand
          }
        })
        $('#editModal').modal()
      }
    },

    openCreateModal(){
      $('#createModal').modal()
    },

    toggleAlert(title, msg, _type){
      this.alert.title = title
      this.alert.message = msg
      this.alert.type = _type
      setTimeout(function(){
        $('#alertSection').removeClass('d-none').addClass('show')
      }, 200)
      setTimeout(function(){
        $('#alertSection').removeClass('show').addClass('d-none')
      }, 5000)
    },

    deleteBrand: function(id){
      let self = this
      axios
        .delete(this.endpoint + id + '/',
          {
            headers: {'csrfmiddlewaretoken': this.csrftoken}
          }
        )
        .then(response => {
          this.brands.map(brand => {
            // if (brand.id == id)
            //   brand.deleted_at = response.data.deleted_at
            self.list_brands()
            $('#deleteModal').modal('hide')
            this.toggleAlert('Disabled!', 
              `the brand ${this.edit_brand.title} successfully.`,
              'alert-warning')
          })
        })
    },

    restoreBrand(){
      axios
        .put(`${this.endpoint}restore/${this.restore_brand.id}`,
          {
            headers: {'csrfmiddlewaretoken': this.csrftoken}
          }
        )
        .then(response => {
          this.brands.map(brand => {
            if (brand.id == this.restore_brand.id)
              brand.deleted_at = null

            $('#restoreModal').modal('hide')
            this.toggleAlert('Enabled!', 
              `the brand ${this.edit_brand.title} successfully.`,
              'alert-success')
          })
        })
    },

    editBrand: function(){
      let fdata = this.serializeEditData()
      axios
        .put(this.endpoint + this.edit_brand.id + '/', 
          fdata,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
              'csrfmiddlewaretoken': this.csrftoken
            }
          }
        )
        .then(response => {
          this.brands.map((brand, index) => {
            if (brand.id == response.data.id)
              this.brands[index] = response.data.brand
          })
          this.imageChanged = false
          $('#editModal').modal('hide')
          this.toggleAlert('Edited!', 
            `the brand ${this.edit_brand.title} successfully.`,
            'alert-success')
        })
    },

    serializeEditData(){
      let fdata = new FormData();
      fdata.append('name', this.edit_brand.name)
      fdata.append('website_url', this.edit_brand.website_url)
      return fdata
    },

    createBrand(){
      let fdata = this.serializeCreateData()
      axios
        .post(this.endpoint,
          fdata,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
              'csrfmiddlewaretoken': this.csrftoken
            }
          }
        )
        .then(response => {
          alert(`Created! the brand ${fdata.get('name')} successfully`)
          location.reload()
        })
    },

    serializeCreateData(){
      let fdata = new FormData();
      fdata.append('name', this.create_brand.name)
      fdata.append('website_url', this.create_brand.website_url)
      return fdata
    },

    enableRestore: function(deleted_at){
      response_color = 'grey'
      if (deleted_at)
        response_color = 'green'
      return response_color
    },

    enableEditDelete: function(deleted_at, color){
      response_color = 'grey'
      if (!deleted_at)
        response_color = color
      return response_color
    },

    setDisable: function(deleted_at, color){
      ret = true
      if (color == 'green' && deleted_at)
        ret = false
      if (color != 'green' && !deleted_at)
        ret = false
      return ret
    },
  },

  created(){
    this.list_brands()
  },

  delimiters: ["[[","]]"]
})