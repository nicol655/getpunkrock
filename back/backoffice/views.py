from django.template import RequestContext
from django.template.context_processors import csrf
from django.contrib.auth import authenticate, login as django_login, logout
from django.shortcuts import render, render_to_response
from rest_framework import generics, status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from backoffice.services import BrandService, ProductService, SubProductService

import json


class SubProductList(APIView):

    """Create a SubProduct."""

    permission_classes = (IsAuthenticated,)
    service = SubProductService
    
    def post(self, request):
        product = self.service().create(request)
        return Response(product)


class SubProductDetails(APIView):

    """Delete or update a SubProduct."""

    permission_classes = (IsAuthenticated,)
    service = SubProductService

    def put(self, request, pk):
        """Update SubProduct."""
        product = self.service().update(request, pk)
        return Response(product)

    def delete(self, request, pk):
        """Delete SubProduct."""
        product = self.service().delete(request, pk)
        return Response(product)


class ProductList(APIView):

    """List all or create a Product."""

    permission_classes = (IsAuthenticated,)
    service = ProductService

    def get(self, request):
        products = self.service().list_all(request)
        return Response(products)
    
    def post(self, request):
        product = self.service().create(request)
        return Response(product)


class ProductDetails(APIView):

    """Update or delete a Product class representation."""

    permission_classes = (IsAuthenticated,)
    service = ProductService

    def put(self, request, pk):
        """Update product."""
        product = self.service().update(request, pk)
        return Response(product)

    def delete(self, request, pk):
        """Delete product."""
        return Response(self.service().delete(request, pk))


class EnableDisableProduct(APIView):

    """Enable or Disable product class representation."""

    permission_classes = (IsAuthenticated,)
    service = ProductService

    def get(self, request, pk):
        """Enable or disable product based on it's current active status."""
        return Response(self.service().enableDisable(request, pk))


class BrandList(APIView):

    """List all or create a Brand."""

    permission_classes = (IsAuthenticated,)
    service = BrandService

    def get(self, request):
        brands = self.service().list_all(request)
        return Response(brands)

    def post(self, request):
        brand = self.service().create(request)
        return Response(brand)


class BrandDetails(APIView):

    """Update or delete Brand instance."""

    permission_classes = (IsAuthenticated,)
    service = BrandService

    def post(self, request, pk):
        if(self.service().put(request, pk)):
            return Response(
                {'message': 'Brand updated'}, 
                status=status.HTTP_204_NO_CONTENT
            )
        return Response(
            {'message': 'Error updating'},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )

    def delete(self, request, pk):
        if(self.service().delete(pk)):
            return Response(
                {'message': 'Brand deleted'}, 
                status=status.HTTP_204_NO_CONTENT
            )
        return Response(
            {'message': 'Error deleting'},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )


class GetEnums(APIView):
        
    """List enums instance."""

    permission_classes = (IsAuthenticated,)
    service = ProductService

    def get(self, request):
        enums = {
            "sizes": self.service().list_sizes(request),
            "categories": self.service().list_categories(request),
            "colors": self.service().list_colors(request),
            "materials": self.service().list_materials(request),
            "genders": self.service().list_genders(request)
        }
        return Response(enums)


@api_view(['GET'])
def logout_view(request):
    """Log out the authenticated user and redirect to login template."""
    if request.user.is_authenticated:
        logout(request)
        return Response({'message': 'Logout success'}, 
                        status=status.HTTP_200_OK)
    return render_to_response("backoffice/404.html")


@api_view(['POST'])
def login_set_user(request):
    """."""
    ret = {
        'user': {
            'name': request.user.username, 
            'admin': request.user.is_superuser, 
            'email': request.user.email
        }
    }
    return Response(ret, status=status.HTTP_200_OK)


def index(request):
    """Render index template for backoffice."""
    return render_to_response("backoffice/index.html")


def error_404(request, exception):
    """Render error_404 template for backoffice."""
    data = {"name": "GetPunkRock"}
    return render(request,'backoffice/404.html', data)