"""Brand service module."""
from json import loads, dumps
import requests


class BrandService:

  url = "http://localhost:8000/api/brands/"

  def list_all(self, request):
    response = requests.get(self.url)
    return response.json()

  def create(self, request):
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    response = requests.post(self.url, data=data)
    return response.json()

  def delete(self, pk):
    return requests.delete('{}{}/'.format(self.url, pk))
  
  def put(self, request, pk):
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    return requests.put('{}{}/'.format(self.url, pk), data=data)
