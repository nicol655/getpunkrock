"""SubProduct service module."""
from json import loads, dumps
import requests


class SubProductService:

  url = "http://localhost:8000/api/sub_products/"

  """Class representation for subproducts services."""

  def create(self, request):
    """Create subproduct."""
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    response = requests.post(self.url, data=data)
    return response.json()

  def update(self, request, pk):
    """Update subproduct."""
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    response = requests.put("{}{}/".format(self.url, pk), data=data)
    return response.json()

  def delete(self, request, pk):
    """Delete subproduct."""
    return requests.delete("{}{}/".format(self.url, pk))