"""Product service module."""
from json import loads, dumps
import requests


class ProductService:

  url = "http://localhost:8000/api/products/"

  def list_all(self, request):
    """List all products service."""
    response = requests.get(self.url)
    return response.json()

  def create(self, request):
    """Create product service."""
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    response = requests.post(self.url, data=data)
    return response.json()

  def update(self, request, pk):
    """Update product service."""
    data = loads(request.data.get('body'))
    data['user_id'] = request.user.pk
    response = requests.put("{}{}/".format(self.url, data['id']),
      data=data)
    return response.json()

  def delete(self, request, pk):
    """Delete product service."""
    response = requests.delete("{}{}/".format(self.url, pk))
    return response.json()

  def enableDisable(self, request, pk):
    """enableDisable product service."""
    response = requests.get("{}{}/enable-disable/".format(self.url, pk))
    return response.json()

  def list_sizes(self, request):
    """List all sizes enum from products service."""
    response = requests.get('{}enums/sizes'.format(self.url))
    return response.json()

  def list_categories(self, request):
    """List all categories enum from products service."""
    response = requests.get('{}enums/categories'.format(self.url))
    return response.json()

  def list_colors(self, request):
    """List all colors enum from products service."""
    response = requests.get('{}enums/colors'.format(self.url))
    return response.json()

  def list_materials(self, request):
    """List all materials enum from products service."""
    response = requests.get('{}enums/materials'.format(self.url))
    return response.json()
    
  def list_genders(self, request):
    """List all genders enum from products service."""
    response = requests.get('{}enums/genders'.format(self.url))
    return response.json()