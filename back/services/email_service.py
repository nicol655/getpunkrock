"""Email Sender service module."""
import os, smtplib, ssl
import base64

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

from templates.emails.welcome_subscriber import (
    template as welcome_subscriber_template)


class EmailSenderService:

    """Email Sender service class representation."""

    def __init__(self):
        """Initializer for email sender service."""
        self.front_host = os.environ.get('FRONT_HOST', 'http://localhost:3001/')
        self.host = os.environ.get('BACK_HOST', 'http://localhost:8000/')
        self.sender_email = os.environ.get('EMAIL_SENDER', 'getpunkrock@gmail.com')
        self.sender_password = os.environ.get('PASSWORD_SENDER', None)
        self.message = MIMEMultipart('related')
        self.message.preamble = 'This is a multi-part message in MIME format.'
        self.recipients = []

    def attach_image(self, image_path, content_path):
        """Attach image to the email message.
        
        Arguments
        ---------
            image_path : str
                image route.
            content_path : str
                content image id.
        """
        fp = open(image_path, 'rb')
        msgImage = MIMEImage(fp.read())
        fp.close()
        msgImage.add_header('Content-ID', content_path)
        self.message.attach(msgImage)

    def set_header(self, subject, recipient):
        """Define email's header.
        
        Arguments
        ---------
            subject : str
                email's subject title.
            recipient : str
                receiver's email address.
        """
        self.message['Subject'] = subject
        self.message['From'] = self.sender_email
        self.message['To'] = recipient

    def set_body(self, template, user_hash):
        """Given the template, assign the user's hash to the unsubscribe
        function.
        
        Arguments
        ---------
            template : str
                whole html template as string.
            user_hash : str
                UUID for the user's hash.
        """
        msgAlternative = MIMEMultipart('alternative')
        self.message.attach(msgAlternative)
        unsubscribe_url = "{}{}{}".format(
            self.front_host, 'cancelar-suscripcion/', user_hash)
        template = template.replace('user_hash', unsubscribe_url)
        msgText = MIMEText(template, 'html')
        msgAlternative.attach(msgText)

    def send_email(self):
        """Login to SMTP_SSL with the credentials and attempt to send an
        email."""
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as s:
            s.login(self.sender_email, self.sender_password)
            s.sendmail(self.sender_email, self.message['To'], 
                       self.message.as_string())

    def send_welcome_subscriber_email(self, name, recipient, user_hash):
        """Set welcome subscriber email and send it to the corresponding
        recipient.
        
        Arguments
        ---------
            name : str
                receiver's first name and last name.
            recipient : str
                receiver's email address.
            user_hash : str
                UUID for the user's hash.
        """
        self.set_header('Suscripción GetPunkRock', recipient)
        template = welcome_subscriber_template.replace('recipient_name', name)
        self.set_body(template, user_hash)
        self.attach_image("templates/images/banner.jpg", "<banner_image>")
        self.attach_image("templates/images/facebook-icon.png", "<facebook_image>")
        self.attach_image("templates/images/instagram-icon.png", "<instagram_image>")
        self.attach_image("templates/images/whatsapp-icon.png", "<whatsapp_image>")
        self.send_email()
