"""Model module for Images."""
from django.db import models


class Image(models.Model):

    """Class model representation for Images."""

    image_url = models.ImageField(upload_to='uploads/images/')
