"""ViewSet Module for Images."""
from rest_framework import viewsets

from .models import Image
from .serializers import ImagesSerializer

import logging


logger = logging.getLogger(__name__)


class ImagesViewSet(viewsets.ModelViewSet):

    """Images ViewSet class representation."""

    queryset = Image.objects.all()
    serializer_class = ImagesSerializer