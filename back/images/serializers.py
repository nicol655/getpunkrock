"""Module Serializer for Images."""
from os import environ
from rest_framework import serializers

from .models import Image


class ImagesSerializer(serializers.ModelSerializer):

    """Images serializer class representation."""

    class Meta:
        model = Image
        fields = ['id', 'image_url']