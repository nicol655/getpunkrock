"""ViewSet Module for products."""
from calendar import monthrange
from datetime import datetime

from django.utils.dateparse import parse_datetime
from django.contrib.auth.models import User
from dateutil.relativedelta import relativedelta
from rest_framework import generics, request, status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from brands.models import Brand
from .enums import CategoryEnum, ColorEnum, MaterialEnum, GenderEnum, SizeEnum
from .models import Image, Product, ProductPerProduct
from .serializers import (ProductImagesSerializer, ProductSerializer,
    ProductPerProductSerializer)

import logging


logger = logging.getLogger(__name__)


class ProductViewSet(viewsets.ModelViewSet):

    """Products ViewSet class representation."""

    queryset = Product.objects.all().filter(deleted_at=None)
    serializer_class = ProductSerializer

    def perform_create(self, serializer):
        """Preset user and brand before create a product."""
        user_id = self.request.data.get('user_id')
        user = User.objects.get(pk=user_id)
        brand = Brand.objects.get(pk=self.request.data.get('brand'))
        serializer.save(created_by=user, brand=brand)

    def destroy(self, request, *args, **kwargs):
        """Override delete function to update the deleted_at field with the
        current datetime."""
        deleted_at = datetime.now()
        product = Product.objects.get(id=kwargs.get('pk'))
        product.deleted_at = deleted_at
        product.save()
        response = {'status': 'product deleted', 'deleted_at': deleted_at}
        return Response(response, status=status.HTTP_200_OK)


class EnableDisableProduct(APIView):

    """Enable or Disable product class representation."""

    def get(self, request, pk):
        """Enable or disable product based on it's current active status."""
        try:
            product = Product.objects.get(pk=pk)
            product.active = not product.active
            product.save()
            response = Response({
                'active': product.active
            }, status=status.HTTP_200_OK)
        except:
            response = Response(
                {'message': 'Error enableing / disableing the product'},
                status=status.HTTP_400_BAD_REQUEST
            )
        return response


class GetProductsFiltered(generics.ListAPIView):
        
    """List products with filters."""

    serializer_class = ProductSerializer
    queryset = Product.objects.filter(deleted_at=None).filter(active=True)

    def get_queryset(self):
        """Returns a list of filtered products."""
        params = self.request.query_params
        new_query = self.queryset.all()
        if params.get("brand"):
            new_query = new_query.filter(brand__name=params.get("brand"))
        if params.get("category"):
            for lbl in CategoryEnum.__labels__:
                if CategoryEnum.__labels__[lbl] == params.get("category"):
                    labl = lbl
            new_query = new_query.filter(category=labl)
        if params.get("gender"):
            for lbl in GenderEnum.__labels__:
                if GenderEnum.__labels__[lbl] == params.get("gender"):
                    labl = lbl
            new_query = new_query.filter(gender=labl)
        return new_query

    def list(self, request, current_page, amount_per_page):
        """Returns a list of paginated products.
        
        Arguments
        ---------
            current_page : str
                number as string with the actual request page.
            amount_per_page : str
                number as string with the amount of products per page to be
                returned.

        Returns
        -------
            dict with the total amount of products and a list with the filter
            and paginated products.
        """
        current_page = int(current_page) - 1
        starts_at = int(amount_per_page) * int(current_page)
        ends_at = starts_at + int(amount_per_page)

        queryset = self.get_queryset()
        serializer = ProductSerializer(queryset, many=True)
        return Response({
            "products": serializer.data[starts_at:ends_at],
            "total": queryset.count()
        })


class GetLatestProducts(generics.ListAPIView):
        
    """List latest created products."""

    serializer_class = ProductSerializer
    queryset = Product.objects.filter(deleted_at=None).filter(active=True)\
        .order_by('-created_at')

    def get_queryset(self):
        """Returns a list with the latest created products, listed by a amount
        gived as param.

        Arguments
        ---------
            amount: int
                number of products to be returned.
        """
        return self.queryset[:int(self.kwargs.get('amount'))]


class ProductsPerProductViewSet(viewsets.ModelViewSet):

    """ProductsPerProduct ViewSet class representation."""

    queryset = ProductPerProduct.objects.all()
    serializer_class = ProductPerProductSerializer

    def perform_create(self, serializer):
        """Preset user before create a SubProduct."""
        user_id = self.request.data.get('user_id')
        user = User.objects.get(pk=1)
        serializer.save(created_by=user)


class ImageViewSet(viewsets.ModelViewSet):

    """ProductImage ViewSet class representation."""

    queryset = Image.objects.all()
    serializer_class = ProductImagesSerializer

    def perform_create(self, serializer):
        """Preset the product before create an image."""
        product = Product.objects.get(id=self.request.data.get('product'))
        serializer.save(
            created_by=product.created_by, 
            product=product)


class GetSizes(APIView):
        
    """List size enum instance."""

    def get(self, request):
        response = []
        labels = SizeEnum.__labels__
        for idx, en in enumerate(SizeEnum.__members__, start=0):
            response.append({'id': idx, 'key': en, 'name': labels[idx]})
        return Response(response)


class GetCategories(APIView):
        
    """List category enum instance."""

    def get(self, request):
        return Response(formatEnum(CategoryEnum.__labels__))


class GetColor(APIView):
        
    """List color enum instance."""

    def get(self, request):
        return Response(formatEnum(ColorEnum.__labels__))


class GetMaterial(APIView):
        
    """List material enum instance."""

    def get(self, request):
        return Response(formatEnum(MaterialEnum.__labels__))


class GetGender(APIView):
        
    """List gender enum instance."""

    def get(self, request):
        return Response(formatEnum(GenderEnum.__labels__))


def formatEnum(labels):
    """Format enums labels into readable dict.

    Arguments
    ---------
        labels - object(integer)
            enums labels return object

    Returns
    -------
        dict with the correct format.
    """
    response = []
    for idx in range(0, len(labels)):
        response.append({'id': idx, 'name': labels[idx]})
    return response