"""Products Enums module."""
from django.utils.translation import ugettext_lazy
from django_enumfield import enum


class SizeEnum(enum.Enum):
    U = 0
    XS = 1
    S = 2
    M = 3
    L = 4
    XL = 5
    XXL = 6

    __labels__ = {
        U: ugettext_lazy("Unique"),
        XS: ugettext_lazy("Extra Small"),
        S: ugettext_lazy("Small"),
        M: ugettext_lazy("Medium"),
        L: ugettext_lazy("Large"),
        XL: ugettext_lazy("Extra Large"),
        XXL: ugettext_lazy("Double Extra Large"),
    }
    __default__ = U


class GenderEnum(enum.Enum):
    MALE = 0
    FEMALE = 1

    __labels__ = {
        MALE: ugettext_lazy("Male"),
        FEMALE: ugettext_lazy("Female"),
    }
    __default__ = MALE


class CategoryEnum(enum.Enum):
    SHIRT = 0
    DRESS = 1
    TOP = 2
    KID = 3
    HOODIE = 4
    SWEATER = 5
    JACKET = 6
    LEGGING = 7
    CROP_TOP = 8
    PANT = 9
    PIJAMA = 10
    SHORT = 11
    DRYFIT = 12
    PLUSHIE = 13
    PILLOW = 14
    STICKER = 15
    ACCESSORY = 16

    __labels__ = {
        SHIRT: ugettext_lazy("Shirt"),
        DRESS: ugettext_lazy("Dress"),
        TOP: ugettext_lazy("Top"),
        KID: ugettext_lazy("Kids"),
        HOODIE: ugettext_lazy("Hoodies"),
        SWEATER: ugettext_lazy("Sweaters"),
        JACKET: ugettext_lazy("Jackets"),
        LEGGING: ugettext_lazy("Leggings"),
        CROP_TOP: ugettext_lazy("Crop Tops"),
        PANT: ugettext_lazy("Pants"),
        PIJAMA: ugettext_lazy("Pijamas"),
        SHORT: ugettext_lazy("Shorts"),
        DRYFIT: ugettext_lazy("Dryfit"),
        PLUSHIE: ugettext_lazy("Plushies"),
        PILLOW: ugettext_lazy("Pillows"),
        STICKER: ugettext_lazy("Stickers"),
        ACCESSORY: ugettext_lazy("Accessories"),
    }
    __default__ = SHIRT


class ColorEnum(enum.Enum):
    BLACK = 0
    WHITE = 1
    RED = 2
    YELLOW = 3
    BLUE = 4

    __labels__ = {
        BLACK: ugettext_lazy("Black"),
        WHITE: ugettext_lazy("White"),
        RED: ugettext_lazy("Red"),
        YELLOW: ugettext_lazy("Yellow"),
        BLUE: ugettext_lazy("Blue"),
    }
    __default__ = WHITE


class MaterialEnum(enum.Enum):
    POLYESTER = 0

    __labels__ = {
        POLYESTER: ugettext_lazy("Polyester"),
    }
    __default__ = POLYESTER