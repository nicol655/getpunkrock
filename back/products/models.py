"""Model module for products."""
from django.db import models
from django.contrib.auth.models import User
from django_enumfield import enum

import uuid

from .enums import CategoryEnum, ColorEnum, MaterialEnum, GenderEnum, SizeEnum
from brands.models import Brand


class Product(models.Model):

    """Class model representation for products."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30, blank=False)
    description = models.CharField(max_length=300, blank=False)
    brand = models.ForeignKey(Brand, related_name="product_brand",
        on_delete=models.DO_NOTHING)
    code = models.CharField(max_length=25, blank=False)
    gender = enum.EnumField(GenderEnum)
    material = enum.EnumField(MaterialEnum)
    color = enum.EnumField(ColorEnum)
    category = enum.EnumField(CategoryEnum)

    created_by = models.ForeignKey(User, related_name='product_created_by',
        on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, related_name='product_modified_by',
        on_delete=models.DO_NOTHING, blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=True, blank=False)


class ProductPerProduct(models.Model):

    """Class model representation for products per products."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Product, related_name='products_per_product',
                                on_delete=models.CASCADE)
    price = models.FloatField(blank=False)
    size = enum.EnumField(SizeEnum)
    stock = models.IntegerField(blank=False)
    discount = models.FloatField(default=0)

    created_by = models.ForeignKey(User, related_name='products_per_product_created_by',
        on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, related_name='products_per_product_modified_by',
        on_delete=models.DO_NOTHING, blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)


class Image(models.Model):

    """Class model representation for Product Images."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Product, related_name='images_per_product',
                               on_delete=models.CASCADE)
    default = models.BooleanField(default=False, blank=False)
    image_url = models.ImageField(upload_to='uploads/products/images/')

    created_by = models.ForeignKey(User, related_name='image_created_by',
        on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, related_name='image_modified_by',
        on_delete=models.DO_NOTHING, blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
