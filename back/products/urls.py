"""URLs module for Product."""
from django.urls import path

from products.views import (EnableDisableProduct, GetSizes, GetCategories,
  GetColor, GetGender, GetMaterial, GetLatestProducts, GetProductsFiltered)


urlpatterns = [
  path('enums/sizes', GetSizes.as_view()),
  path('enums/categories', GetCategories.as_view()),
  path('enums/colors', GetColor.as_view()),
  path('enums/materials', GetMaterial.as_view()),
  path('enums/genders', GetGender.as_view()),
  path('<pk>/enable-disable/', EnableDisableProduct.as_view()),
  path('latest_created/<amount>/', GetLatestProducts.as_view()),
  path('<current_page>/<amount_per_page>', GetProductsFiltered.as_view()),
]