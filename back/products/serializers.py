"""Module Serializer for Products."""
from os import environ
from rest_framework import serializers

from brands.models import Brand
from brands.serializers import BrandSerializer

from .models import Image, Product, ProductPerProduct


class ProductImagesSerializer(serializers.ModelSerializer):

    """ProductImages serializer class representation."""

    class Meta:
        model = Image
        fields = ['id', 'image_url', 'product']


class ProductPerProductSerializer(serializers.ModelSerializer):

    """ProductPerProduct serializer class representation."""

    total = serializers.SerializerMethodField()
    sizeString = serializers.SerializerMethodField()
    sizeKey = serializers.SerializerMethodField()

    class Meta:
        model = ProductPerProduct
        fields = ["id", "price", "sizeString", "sizeKey", "size", "stock",
            "discount", "total", "product"]

    def get_sizeString(self, obj):
        """Get the size Enum label."""
        return obj.size.label

    def get_sizeKey(self, obj):
        """Get the size Enum label."""
        return obj.size.name

    def get_total(self, obj):
        """Calculate the total amount with applied discount."""
        return obj.price - ((obj.discount / 100) * obj.price)


class ProductSerializer(serializers.ModelSerializer):

    """Product Serializer class representation."""

    images = serializers.SerializerMethodField()
    brandName = serializers.SerializerMethodField()
    categoryString = serializers.SerializerMethodField()
    colorString = serializers.SerializerMethodField()
    materialString = serializers.SerializerMethodField()
    genderString = serializers.SerializerMethodField()
    subProducts = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            'id', 'name', 'description', 'category', 'categoryString', 'color',
            'colorString', 'brand', 'brandName', 'material', 'materialString',
            'gender', 'genderString', 'code', 'images', 'subProducts', 'active'
        ]

    def get_categoryString(self, obj):
        """Get the Category Enum label."""
        return obj.category.label

    def get_materialString(self, obj):
        """Get the material Enum label."""
        return obj.material.label

    def get_colorString(self, obj):
        """Get the color Enum label."""
        return obj.color.label

    def get_genderString(self, obj):
        """Get the gender Enum label."""
        return obj.gender.label

    def get_images(self, obj):
        """Get images for product. If there is not images, returns an empty
        list."""
        query_set = Image.objects.filter(product=obj.id)
        return ProductImagesSerializer(query_set, many=True).data

    def get_subProducts(self, obj):
        """Get subProducts for product. If there is not subProducts, returns
        an empty list."""
        query_set = ProductPerProduct.objects.filter(product=obj.id).order_by("size")
        return ProductPerProductSerializer(query_set, many=True).data

    def get_brandName(self, obj):
        """Get the brand name."""
        return obj.brand.name